<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function(Blueprint $table){

            $table->increments('id');
            $table->string('title');
            $table->string('type');
            $table->date('date');
            $table->time('time');
            $table->longText('comments')->nullable();
            $table->integer('lead_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('status')->default('OPEN');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}

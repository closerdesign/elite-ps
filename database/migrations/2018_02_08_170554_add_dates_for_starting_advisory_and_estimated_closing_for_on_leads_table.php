<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatesForStartingAdvisoryAndEstimatedClosingForOnLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function($table){

            $table->date('advisory_start_date')->nullable();
            $table->date('estimated_closing_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function($table){

            $table->dropDolumn('advisory_start_date');
            $table->dropColumn('estimated_closing_date');

        });
    }
}

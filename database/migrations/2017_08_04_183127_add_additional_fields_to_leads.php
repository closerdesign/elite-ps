<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function($table){

            $table->longText('procedures')->nullable();
            $table->string('currency')->nullable();
            $table->integer('amount')->nullable();
            $table->longText('additional_comments')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function($table){

            $table->dropColumn('procedures');
            $table->dropColumn('currency');
            $table->dropColumn('amount');
            $table->dropColumn('additional_comments');

        });
    }
}

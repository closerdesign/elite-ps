<?php

namespace App\Console\Commands;

use App\Mail\DueTask;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class TaskReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending task reminders via email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $time = date('H:i:00');

        $users = User::whereHas('tasks', function($query) use ($date,$time){
            $query->where('date', $date)
                ->where('time', $time);
        })->with(['tasks' => function($query) use ($date, $time){
            $query
                ->where('date', $date)->where('time', $time); }])
            ->get();

        foreach ($users as $user)
        {
            Mail::to($user->email)->send(new DueTask($user));
        }
    }
}

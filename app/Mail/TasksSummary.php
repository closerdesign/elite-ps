<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TasksSummary extends Mailable
{
    use Queueable, SerializesModels;

    public $tasks;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tasks, $user)
    {
        $this->tasks = $tasks;

        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.messages.tasks-summary')->subject('[' . date('Y-m-d') . '] Resumen de Tareas Pendientes');
    }
}

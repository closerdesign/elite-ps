<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'is_assigner', 'active', 'team',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *  User Tasks
     *
     *
     */

    public function tasks()
    {
        return $this->hasManyThrough('App\Task', 'App\Lead');
    }

    /**
     *  Admin Role
     *
     *
     */

    public function isAdmin()
    {
        $admin = false;

        if($this->is_admin == 'Si')
        {
            $admin = true;
        }

        return $admin;
    }

    /**
     *  Get Admin Attr
     *
     *
     */

    public function getIsAdminAttribute($value)
    {
        $admin = 'No';

        if($value == 1)
        {
            $admin = 'Si';
        }

        return $admin;
    }

    /**
     *  Assigner Role
     *
     *
     */

    public function isAssigner()
    {
        $assigner = false;

        if($this->is_assigner == 'Si')
        {
            $assigner = true;
        }

        return $assigner;
    }

    /**
     *  Get Admin Attr
     *
     *
     */

    public function getIsAssignerAttribute($value)
    {
        $assigner = 'No';

        if($value == 1)
        {
            $assigner = 'Si';
        }

        return $assigner;
    }


}

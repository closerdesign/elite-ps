<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CommentFile extends Model
{
    protected $fillable = [
        'file',
        'comment_id'
    ];

    public function getFileAttribute($value)
    {
        return Storage::url('comments-files/' . $value);
    }
}

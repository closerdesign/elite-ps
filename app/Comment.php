<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'comments',
        'file',
        'lead_id'
    ];

    public function lead()
    {
        return $this->belongsTo('App\Lead');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function files()
    {
        return $this->hasMany('App\CommentFile');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Lead extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'country',
        'comments',
        'whatsapp',
        'user_id',
        'status',
        'procedures',
        'currency',
        'amount',
        'additional_comments',
        'language',
        'quote',
        'destination',
        'advisory_start_date',
        'estimated_closing_date',
        'source',
        'assigned_at'
    ];

    public function getLanguageAttribute($value)
    {
        $language = "Español";

        if($value != 'es')
        {
            $language = "English";
        }

        return $language;
    }

    public function getQuoteAttribute($value)
    {
        $quote = "";

        if($value != "")
        {
            $quote = Storage::url('quotes/' . $value);
        }

        return $quote;
    }

    public function getWhatsappAttribute($value)
    {
        $whatsapp = "No";

        if($value == 1)
        {
            $whatsapp = "Si";
        }

        return $whatsapp;
    }

    public function setQuoteAttribute($value)
    {
        $this->attributes['quote'] = (string) $value ?: null;
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    /**
     *  Pending Status
     *
     *
     */

    public function scopePending($query)
    {
        return $query
            ->where('status', 'PENDING')
            ->where(function($q){
                $q->whereNull('quote')
                    ->orWhere('quote', '');
            });
    }

    public function scopeUnresponsive($query)
    {
        return $query
            ->where('status', 'UNRESPONSIVE')
            ->where(function($q){
                $q->whereNull('quote')
                    ->orWhere('quote', '');
            });
    }

    public function scopeOpen($query)
    {
        return $query
            ->where('status', 'OPEN')
            ->where(function($q){
                $q->whereNull('quote')
                    ->orWhere('quote', '');
            });
    }

    public function scopeInProgress($query)
    {
        return $query->whereIn('status', ['OPEN','PENDING','UNRESPONSIVE']);
    }

    /**
     *  Quote Status
     *
     *
     */

    public function scopeQuote($query)
    {
        return $query
            ->whereIn('status', [
                'OPEN',
                'UNRESPONSIVE',
                'PENDING'
            ])
            ->where(function($q){
                $q->whereNotNull('quote')
                    ->orWhere('quote', '!=', '');
            });
    }

    public function scopeClosed($query)
    {
        return $query
            ->where('status', 'CLOSED');
    }

    public function scopeCompleted($query)
    {
        return $query
            ->where('status', 'COMPLETED');
    }

    public function scopeDiscarded($query)
    {
        return $query
            ->where('status', 'DISCARDED');
    }

    public function scopeLost($query)
    {
        return $query
            ->where('status', 'LOST');
    }

    public function scopePostponed($query)
    {
        return $query
            ->where('status', 'POSTPONED');
    }

    /**
     *  Advisor Relationship
     *
     *
     */

    public function advisor()
    {
        return $this->belongsTo('App\User', 'user_id', 'id', 'users');
    }



}

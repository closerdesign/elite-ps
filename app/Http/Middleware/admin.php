<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ( !Auth::check() || !Auth::user()->isAdmin() )
        {
            Session::flash('message', [
                'type'    => 'danger',
                'message' => 'Acceso Restringido'
            ]);

            return redirect('/home');
        }

        return $next($request);

    }
}

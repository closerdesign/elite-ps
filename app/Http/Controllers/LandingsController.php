<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Lang;
use Meta;

class LandingsController extends Controller
{
    /**
     *  Puerto Rico
     *
     *
     */

    public function puerto_rico()
    {
        Meta::set('title', Lang::get('landings/puerto-rico.title'));
        Meta::set('description', Lang::get('landings/puerto-rico.description'));

        return view('pages.landings.puerto-rico');
    }
}

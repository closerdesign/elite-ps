<?php

namespace App\Http\Controllers;

use App\Lead;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = null)
    {
        if($status)
        {
            $leads = Lead::$status();

        }else{

            $leads = Lead::pending();
        }

        $leads = $leads->where('user_id', Auth::user()->id)->paginate(25);

        $tasks = Task::pending(Auth::user()->id)->paginate(12);

        return view('home', compact('leads', 'tasks', 'status'));
    }
}

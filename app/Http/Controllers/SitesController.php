<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SitesController extends Controller
{
    public function index()
    {
        App::setLocale('es');

        $nav = 1;

        return view('sites.cirugiaplasticabogota-org.index', compact('nav'));
    }

    public function before_after()
    {
        App::setLocale('es');

        $nav = 1;

        return view('pages.before-after', compact('nav'));
    }

    public function contact()
    {
        App::setLocale('es');

        $nav = 1;

        return view('pages.contact', compact('nav'));
    }
}

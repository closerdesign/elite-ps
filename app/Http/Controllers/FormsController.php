<?php

namespace App\Http\Controllers;

use App\Lead;
use App\Mail\Leads;
use Hocza\Sendy\Facades\Sendy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FormsController extends Controller
{
    /**
     *  Leads Form
     *
     *
     */

    public function leads(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'country' => 'required',
        ]);

        $lead = new Lead($request->all());

        $lead->comments = $request->comments;

        $lead->comments .= " [" . $_SERVER['HTTP_REFERER'] . "]";

        $lead->save();

        Mail::to('contacto@elitecirugiaplastica.org')
            ->send(new Leads($lead));

        $list = "ksl892tYGWTQzgZylmMyuZ2Q";

        if( App::getLocale() == 'en' )
        {
            $list = "sDB2qxOby7umbOItKZhkdQ";
        }

        Sendy::setListId($list)->subscribe([
            'name' => $lead->name,
            'email' => $lead->email
        ]);

        Session::flash('message', [
            'type' => 'success',
            'message' => Lang::get('forms.leads')
        ]);

        Session::flash('conversion', '<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1001969282/?label=BlovCNq58m8Qgq3j3QM&amp;guid=ON&amp;script=0"/>');

        Cookie::queue('lead', 1, 6*30*24*3600);

        return back();
    }
}

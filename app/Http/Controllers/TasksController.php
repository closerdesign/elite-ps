<?php

namespace App\Http\Controllers;

use App\Mail\TasksSummary;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'type'  => 'required',
            'date'  => 'required',
            'time'  => 'required',
        ]);

        $task = new Task($request->all());

        $task->user_id = Auth::user()->id;

        $task->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => '¡Tarea creada exitosamente!'
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);

        $task->fill($request->all());

        $task->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => '¡Buen Trabajo!'
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Daily Summary
     *
     *
     */

    public static function summary()
    {
        foreach (User::all() as $user)
        {
            $tasks = Task::pending($user->id)->get();

            if( count($tasks) > 0 )
            {
                Mail::to($user->email)
//                    ->bcc('juanc@closerdesign.co')
                    ->send(new TasksSummary($tasks, $user));
            }
        }
    }
}

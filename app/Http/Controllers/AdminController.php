<?php

namespace App\Http\Controllers;

use App\Lead;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     *  Transfer View
     *
     *
     */

    public function transfer_view()
    {
        return view('admin.transfer');
    }

    /**
     *  Transfer Leads
     *
     *
     */

    public function transfer(Request $request)
    {
        $this->validate($request, [
            'from' => 'required|integer',
            'to' => 'required|integer'
        ]);

        $from = User::findOrFail($request->from);

        $to = User::findOrFail($request->to);

        $count = Lead::where('user_id', $from)->count();

        DB::table('leads')
            ->where('user_id', $from->id)
            ->update([
                'user_id' => $to->id
            ]);

        Session::flash('message', [
            'type'    => 'success',
            'message' => $count . ' contactos han sido transferidos de manera exitosa.'
        ]);

        return back();
    }

    /**
     *  Switch Users View
     *
     *
     */

    public function switch_user_view()
    {
        return view('admin.switch-user');
    }

    /**
     *  Switch User
     *
     *
     */

    public function switch_user($id)
    {
        Auth::loginUsingId($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Usuario Activo Ha Sido Cambiado'
        ]);

        return redirect()->action('HomeController@index');
    }

    /**
     *  Users View
     *
     *
     */

    public function users()
    {
        $users = User::orderBy('name')->get();

        return view('admin.users', compact('users'));
    }

    /**
     *  User Details
     *
     *
     */

    public function user_details($id)
    {
        $user = User::findOrFail($id);

        return view('admin.user-details', compact('user'));
    }

    /**
     *  User Update
     *
     *
     */

    public function user_update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'required|email',
            'is_admin' => 'required|boolean',
            'is_assigner' => 'required|boolean'

        ]);

        $user->fill($request->all());

        $user->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Usuario Actualizado Exitosamente'
        ]);

        return redirect()->action('AdminController@users');
    }

    /**
     *  User Delete
     *
     *
     */

    public function user_delete($id)
    {
        User::destroy($id);

        Session::flash('message', [
            'type' => 'success',
            'message' => 'Usuario Eliminado Exitosamente'
        ]);

        return redirect()->action('AdminController@users');
    }
}

<?php

namespace App\Http\Controllers;

use App\Lead;
use App\Mail\LeadAssignment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use JeroenDesloovere\VCard\VCard;
use Yajra\Datatables\Facades\Datatables;

class LeadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin', ['only' => ['allocate', 'destroy', 'switch-user']]);

        $this->middleware('auth', ['except' => ['vcard']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('leads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'nullable|email',
            'phone'   => 'required|numeric|digits:11',
            'country' => 'required',
            'whatsapp' => 'required|boolean'
        ]);

        $lead = new Lead($request->all());

        if( ( Auth::user()->isAssigner() != true ) && ( Auth::user()->isAdmin() == true ) )
        {
            $lead->user_id = 0;
        }

        $lead->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => '¡Buen trabajo! Tu contacto ha sido creado correctamente.'
        ]);

        if( ( Auth::user()->isAssigner() != true ) && ( Auth::user()->isAdmin() == true ) )
        {
            return redirect()->action('LeadsController@create');
        }

        return redirect()->action('LeadsController@show', $lead->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lead = Lead::findOrFail($id);

        return view('leads.show', compact('lead'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lead = Lead::findOrfail($id);

        $quote = basename($lead->quote);

        if( $request->hasFile('quote') )
        {
            $quote = uniqid() . '.' . $request->file('quote')->getClientOriginalExtension();

            Storage::put('quotes/' . $quote, file_get_contents($request->file('quote')->getRealPath()), 'public');
        }

        $lead->fill($request->all());

        $lead->quote = $quote;

        $lead->save();

        Session::flash('message', [
            'type'    => 'success',
            'message' => '¡Oportunidad Actualizada Exitosamente!'
        ]);

        if($request->assign == 1)
        {
            $user = User::findOrFail($request->user_id);

            $lead->status = 'PENDING';

            $lead->assigned_at = Carbon::now();

            $lead->save();

            Mail::to($user->email)->send(new LeadAssignment($lead));

            return redirect('/home');
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lead::destroy($id);

        Session::flash('message', [
            'type'    => 'success',
            'message' => 'Lead eliminado en forma exitosa.'
        ]);

        return back();
    }

    /**
     *  Contacts Allocation
     *
     *
     */

    public function allocate()
    {
        return view('leads.allocate');
    }

    /**
     *  Contacts Allocation Data
     *
     *
     */

    public function allocate_data()
    {
        $advisors = "";

        foreach (User::orderBy('name')->get() as $user)
        {
            $count = count(\App\Lead::where( function ($query) use ($user){ $query->where('user_id', $user->id)->where('status', ['OPEN', 'POSTPONED']); } )->get());

            $advisors .= "<option value='$user->id'>$user->name ($count)</option>";
        }

        $leads = Lead::where(function($q){ $q->where('user_id', 0); })->select(['id', 'name', 'email', 'phone', 'country', 'whatsapp', 'language', 'created_at']);

        return Datatables::of($leads)
            ->addColumn('action', function($lead) use ($advisors){

                return "

                <form action='" . action('LeadsController@update', $lead->id) . "' method='post'>
                " . csrf_field() . method_field('PATCH') . "
                <input type='hidden' name='assign' value='1'>
                <div class='input-group'>
                    <select name='user_id' id='user_id' class='form-control' required >
                        <option value=''>Asignar a...</option>" . $advisors . "
                    </select>
                    <span class='input-group-btn'>
                        <button class='btn btn-success'>
                            <i class='fa fa-arrow-circle-right'></i>
                        </button>
                    </span>
                </div>
                </form>

                ";

            })->make(true);
    }

    /**
     *  Leads Search
     *
     *
     */

    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required'
        ]);

        $keyword = $request->keyword;

        $leads = Lead::where(function($q) use ($request){
            $q->where('name', 'like', '%' . $request->keyword . '%')
                ->orWhere('email', 'like', '%' . $request->keyword . '%')
                ->orWhere('phone', 'like', '%' . $request->keyword . '%');
        })->where('status', '!=', 'ARCHIVED')
            ->paginate(100);

        if( !Auth::user()->isAdmin() )
        {
            $leads = $leads->where('user_id', Auth::user()->id);
        }

        return view('leads.search', compact('leads', 'keyword'));
    }

    /**
     *  Tutorial
     *
     *
     */

    public function tutorial()
    {
        return view('leads.tutorial');
    }

    /**
     *  Report
     *
     *
     */

    public function report()
    {
        return view('leads.report');
    }

    /**
     *  Assignee
     *
     *
     */

    public function assignee()
    {
        return view('leads.assignee');
    }

    /**
     *  VCard
     *
     *
     */

    public function vcard($id)
    {
        $lead = Lead::findOrFail($id);

        $lastname = '';
        $firstname = $lead->name;
        $additional = '';
        $prefix = '';
        $suffix = '';

        $vcard = new VCard();
        $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);
        $vcard->addEmail($lead->email);
        $vcard->addPhoneNumber($lead->phone, 'PREF;WORK');

        return response($vcard->getOutput(), 200)
            ->withHeaders([
                'Content-Type' => 'text/x-vcard'
            ]);
    }
}

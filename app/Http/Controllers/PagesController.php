<?php

namespace App\Http\Controllers;

use Illuminate\Cookie\CookieJar;
use Illuminate\Support\Facades\Cookie;
use Meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class PagesController extends Controller
{
    protected $prices;

    public function __construct()
    {
        $this->prices = [

            'general' => [
                'en' => [
                    'package-1' => [
                        'USD' => 11800,
                        'EUR' => 9500
                    ],
                    'package-2' => [
                        'USD' => 8800,
                        'EUR' => 7200
                    ],
                    'package-3' => [
                        'USD' => 8800,
                        'EUR' => 7200
                    ],
                    'package-4' => [
                        'USD' => 6500,
                        'EUR' => 5500
                    ],
                    'package-5' => [
                        'USD' => 6500,
                        'EUR' => 5500
                    ],
                    'package-6' => [
                        'USD' => 5800,
                        'EUR' => 5400
                    ],
                    'package-7' => [
                        'USD' => 5800,
                        'EUR' => 5400
                    ],
                    'package-8' => [
                        'USD' => 8500,
                        'EUR' => 6500
                    ],
                    'package-9' => [
                        'USD' => 5500,
                        'EUR' => 6500
                    ],
                    'package-10' => [
                        'USD' => 350,
                        'EUR' => 350
                    ],
                    'package-11' => [
                        'USD' => 8500,
                        'EUR' => 7500
                    ]
                ],
                'es' => [
                    'package-1' => [
                        'USD' => 10800,
                        'EUR' => 9500
                    ],
                    'package-2' => [
                        'USD' => 7800,
                        'EUR' => 7200
                    ],
                    'package-3' => [
                        'USD' => 7800,
                        'EUR' => 7200
                    ],
                    'package-4' => [
                        'USD' => 5500,
                        'EUR' => 4500
                    ],
                    'package-5' => [
                        'USD' => 5500,
                        'EUR' => 4500
                    ],
                    'package-6' => [
                        'USD' => 4800,
                        'EUR' => 4400
                    ],
                    'package-7' => [
                        'USD' => 4800,
                        'EUR' => 4400
                    ],
                    'package-8' => [
                        'USD' => 7500,
                        'EUR' => 6500
                    ],
                    'package-9' => [
                        'USD' => 6500,
                        'EUR' => 5500
                    ],
                    'package-10' => [
                        'USD' => 350,
                        'EUR' => 350
                    ],
                    'package-11' => [
                        'USD' => 7500,
                        'EUR' => 6500
                    ]
                ],

            ],
            'south-america' => [
                'package-3' => [
                    'USD' => 8100
                ],
                'package-2' => [
                    'USD' => 6100
                ],
                'package-1' => [
                    'USD' => 3800
                ]
            ],
            'central-america' => [
                'package-3' => [
                    'USD' => 8600,
                ],
                'package-2' => [
                    'USD' => 6600
                ],
                'package-1' => [
                    'USD' => 4000
                ]
            ]

        ];
    }

    /**
     *  Home Page Display
     *
     *
     */

    public function home()
    {
        Meta::set('title', Lang::get('general.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.home');
    }

    /**
     *  Testimonials
     *
     *
     */

    public function testimonials()
    {
        Meta::set('title', Lang::get('testimonials.title'));
        Meta::set('description', Lang::get('general.description'));

        $videos = [
	        1 => ['country' => 'Australia', 'video'   => '0YpUIh3HRcw'],
	        2 => ['country' => 'Canada', 'video'   => 'X92z_a5rPoE'],
	        3 => ['country' => 'Chile', 'video'   => 'DIYhCRdQgrQ'],
	        4 => ['country' => 'USA', 'video'   => 'AZQNA58slv4'],
	        5 => ['country' => 'USA', 'video'   => '2tOyF7qeXGA'],
	        6 => ['country' => 'Puerto Rico', 'video'   => 'yQHC2euIPlg'],
            7 => ['country' => 'USA', 'video'   => 'jZPYmDdNY4g'],
            8 => ['country' => 'Chile', 'video'   => 'PnQr6ytZGb8'],
            9 => ['country' => 'Costa Rica', 'video'   => 'QqfxuiYWaPI'],
            10 => ['country' => 'Puerto Rico', 'video'   => 'Z1MJkNoxkLc'],
            11 => ['country' => 'USA', 'video'   => 'C7KJB4AzGXI'],
            12 => ['country' => 'USA', 'video'   => 'of1VhHN6Fsw'],
            13 => ['country' => 'Alemania', 'video'   => 'LP_F97hB0wo'],
            14 => ['country' => 'Suiza', 'video'   => 'PuP-IesaFMo'],
            15 => ['country' => 'Cuba', 'video'   => 'KncG21FYMCU'],
            16 => ['country' => 'USA', 'video'   => 'ADDip6EeuVA'],
            17 => ['country' => 'España', 'video'   => 'g2H3MFDdGdM'],
            18 => ['country' => 'Puerto Rico', 'video'   => '29t4p9myDm0'],
            19 => ['country' => 'Venezuela', 'video'   => 'meLZP71Hh4Q'],
            20 => ['country' => 'USA', 'video'   => 'lrz3nTV4Nmw'],
            21 => ['country' => 'Puerto Rico', 'video'   => 'mcGoOl6WbAg'],
            22 => ['country' => 'Curacao', 'video'   => 'xC3oFtsNRk8'],
            23 => ['country' => 'Chile', 'video'   => 'lg-7LeWHkv0'],
            24 => ['country' => 'España', 'video'   => 'Ffyhb5sIV1Q'],
            25 => ['country' => 'Canada', 'video'   => 'qUfn2sNuIwQ'],
            26 => ['country' => 'USA', 'video'   => 'JvIzR_rwAJI'],
            27 => ['country' => 'USA', 'video'   => 'N6UFAsruPNE'],
            28 => ['country' => 'USA', 'video'   => '9nJzTf_Wfok'],
            29 => ['country' => 'Puerto Rico', 'video'   => '2xSvBNrlFPs'],
            30 => ['country' => 'Aruba', 'video'   => 'r5vshYHhxng'],
            31 => ['country' => 'Rep. Dominicana', 'video'   => '_Jwg8VsEZrs'],
        ];

        return view('pages.testimonials', compact('videos'));
    }

    /**
     *  Before After Galleries
     *
     *
     */

    public function before_after()
    {
        Meta::set('title', Lang::get('before-after.title'));
        Meta::set('description', Lang::get('general.description'));

        $galleries = [
            1 => [
                'name' => Lang::get('before-after.lipoabdominoplasty'),
                'folder' => 'lipoabdominoplastia',
                'images' => []
            ],
            2 => [
                'name' => Lang::get('before-after.liposculpture'),
                'folder' => 'lipoescultura',
                'images' => []
            ],
            3 => [
                'name' => Lang::get('before-after.breast-augmentation-and-lift'),
                'folder' => 'aumento-y-levantamiento',
                'images' => []
            ],
            4 => [
                'name' => Lang::get('before-after.rhinoplasty'),
                'folder' => 'rinoplastia',
                'images' => []
            ]

        ];

        foreach ($galleries as $key => $val)
        {
            $files = glob(public_path() . '/img/galleries/' . $val['folder'] . '/*.JPG');

            foreach ($files as $file)
            {
                array_push($galleries[$key]['images'], basename($file));
            }
        }

        return view('pages.before-after', compact('galleries'));
    }

    /**
     *  Prices Section
     *
     *
     */

    public function prices()
    {
        $prices = $this->prices;

        $region = 'general';

        $locale = [
            'en' => 170,
            'es' => 168,
            'fr' => 170
        ];

        $products = 'https://closerdesign.net/api/product-category/' . $locale[App::getLocale()];
        $products = file_get_contents($products);
        $products = json_decode($products)[0];

        Meta::set('title', Lang::get('prices.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.prices.' . App::getLocale(), compact('prices', 'region', 'products'));
    }

    /**
     *  Region
     *
     *
     */

    public function region(CookieJar $cookieKar, $id)
    {
        $cookieKar->queue('region', $id, time() + (10 * 365 * 24 * 60 * 60));

        $prices = $this->prices;

        $region = $id;

        Meta::set('title', Lang::get('prices.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.prices.' . App::getLocale(), compact('prices', 'region'));
    }

    /**
     *  Colombia Section
     *
     *
     */

    public function colombia()
    {
        Meta::set('title', 'Colombia - ' . Lang::get('general.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.colombia');
    }

    /**
     *  Contact
     *
     *
     */

    public function contact()
    {
        Meta::set('title', Lang::get('contact.title'));
        Meta::set('description', Lang::get('contact.description'));

        return view('pages.contact');
    }

    /**
     *  About
     *
     *
     */

    public function about()
    {
        Meta::set('title', Lang::get('about.title'));
        Meta::set('description', Lang::get('about.description'));

        return view('pages.about');
    }

    /**
     *  Why Elite?
     *
     *
     */

    public function why_elite()
    {
        Meta::set('title', Lang::get('why-elite.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.why-elite');
    }

    /**
     *  Security
     *
     *
     */

    public function security()
    {
        Meta::set('title', Lang::get('security.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.security');
    }

    /**
     *  Arrival
     *
     *
     */

    public function arrival()
    {
        Meta::set('title', Lang::get('reception.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.arrival');
    }

    /**
     *  Hosting
     *
     *
     */

    public function hosting()
    {
        Meta::set('title', Lang::get('hosting.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.hosting');
    }

    /**
     *  Advisors
     *
     *
     */

    public function advisors()
    {
        Meta::set('title', Lang::get('advisors.title'));
        Meta::set('description', Lang::get('general.description'));

        return view('pages.advisors');
    }

    /**
     *  Blog
     *
     *
     */

    public function blog()
    {
        Meta::set('title', 'Preguntas y Respuestas Cirugía Plástica');
        Meta::set('description', Lang::get('general.description'));

        $posts = file_get_contents('http://www.closerdesign.net/api/posts/12');

        if(isset($_GET['page']))
        {
            $posts = file_get_contents('http://www.closerdesign.net/api/posts/12?page=' . $_GET['page']);
        }

        $posts = json_decode($posts);

        return view('pages.blog', compact('posts'));
    }

    /**
     *  Post
     *
     *
     */

    public function post($id)
    {
        $post = file_get_contents('http://www.closerdesign.net/api/post/' . $id);

        $post = json_decode($post);

        Meta::set('title', $post->title);
        Meta::set('description', strip_tags($post->content));
        Meta::set('image', $post->image);

        return view('pages.post', compact('post'));
    }
}

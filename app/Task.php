<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title',
        'type',
        'date',
        'time',
        'comments',
        'lead_id',
        'status'
    ];

    public function getStatusAttribute($value)
    {
        $status = "PENDIENTE";

        if( $value == "OPEN" && ($this->date <= date('Y-m-d') && (date('h:i A', strtotime($this->time)) < date('h:i A') ) ) )
        {
            $status = "VENCIDA";
        }

        if($value == 'COMPLETED')
        {
            $status = "COMPLETADA";
        }

        return $status;
    }

    public function getTypeAttribute($value)
    {
        $type = [
            'CALL'    => 'LLAMADA',
            'EMAIL'   => 'EMAIL',
            'MEETING' => 'REUNIÓN',
            'OTHER'   => 'OTRO'
        ];

        return $type[$value];
    }

    public function lead()
    {
        return $this->belongsTo('App\Lead');
    }

    public function scopePending($query, $user)
    {
        $query->where('user_id', $user)
            ->where('status', '!=', 'COMPLETED')
            ->orderBy('date')
            ->orderBy('time');
    }
}

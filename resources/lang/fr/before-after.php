<?php

return [

    'title' => 'Galeries: Avant et Après',

    'cta' => 'Pour la confidentialité de notre site, afin de pouvoir visualiser nos galeries “avant-après”, vous devez premièrement compléter notre formulaire de conseillerie immédiate.',

    'lipoabdominoplasty' => 'Lipoabdominoplastie',
    'liposculpture' => 'Liposculpture',
    'breast-augmentation-and-lift' => 'Augmentation et levage mammaire',
    'rhinoplasty' => 'Rhinoplastie',

];
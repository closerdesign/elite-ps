<?php

return [

    'whatsapp' => 'Whatsapp',
    'call-us-now' => '¡Appelez nous maintenant!',
    'usa' => 'Les États-Unis',
    'spain' => 'L’Espagne',
    'puerto-rico' => 'Le Puerto Rico',
    'canada' => 'Le Canada',
    'panama' => 'Le Panama',
    'chile' => 'Le Chili',
    'costa-rica' => 'Le Costa Rica',
    'australia' => 'l’Australie',

];
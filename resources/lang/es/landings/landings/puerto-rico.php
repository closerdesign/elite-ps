<?php

return [

    'title'       => 'Paquetes Especiales desde Puerto Rico',
    'description' => 'Disfruta de nuestros paquetes diseñados especialmente para nuestros pacientes proveniente de la bella isla de Puerto Rico!',

];
<?php

return [

    'title' => 'Alojamiento',
    
    'title-bogota' => 'Alojamiento en bogotá',
    
    'title-cartagena' => 'Alojamiento en cartagena',
    
    'custom-video-bogota' => '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/vgDNuod517Q?autoplay=0&rel=0"></iframe>',
    
    'custom-video-cartagena' => '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qJX4HM4nCw0?autoplay=0&rel=0"></iframe>',

];
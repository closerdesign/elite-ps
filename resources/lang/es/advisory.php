<?php

return [

    'whatsapp' => 'Vía Whatsapp',
    'call-us-now' => '¡Llámanos Ahora!',
    'usa' => 'Desde Estados Unidos',
    'spain' => 'Desde España',
    'puerto-rico' => 'Desde Puerto Rico',
    'canada' => 'Desde Canada',
    'panama' => 'Desde Panamá',
    'chile' => 'Desde Chile',
    'costa-rica' => 'Desde Costa Rica',
    'australia' => 'Desde Australia',

];
<?php

return [

    'title' => 'Galerias: Antes y Después',

    'cta' => 'Por razones de privacidad, para poder visualizar nuestras galerias de Antes y Después, debe antes diligenciar sus datos en nuestro formulario de Asesoría Inmediata',

    'lipoabdominoplasty' => 'Lipoabdominoplastia',
    'liposculpture' => 'Lipoescultura',
    'breast-augmentation-and-lift' => 'Aumento y Levantamiento de Senos',
    'rhinoplasty' => 'Rinoplastia',

];
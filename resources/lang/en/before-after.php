<?php

return [

    'title' => 'Before & After Galleries',

    'cta' => 'For privacy reasons, in order to view our Before / After galleries, you need to fill out our Instant Advisory form',

    'lipoabdominoplasty' => 'Lipoabdominoplasty',
    'liposculpture' => 'Liposculpture',
    'breast-augmentation-and-lift' => 'Breast Augmentation And Lift',
    'rhinoplasty' => 'Rhinoplasty',

];
<?php

return [

    'whatsapp' => 'Via Whatsapp',
    'call-us-now' => 'Call Us Now!',
    'usa' => 'From USA',
    'spain' => 'From Spain',
    'puerto-rico' => 'From Puerto Rico',
    'canada' => 'From Canada',
    'panama' => 'From Panama',
    'chile' => 'From Chile',
    'costa-rica' => 'From Costa Rica',
    'australia' => 'From Australia',

];
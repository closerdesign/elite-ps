<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Elite | Marketing Platform</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/js/pickadate/lib/themes/classic.css">
    <link rel="stylesheet" href="/js/pickadate/lib/themes/classic.date.css">
    <link rel="stylesheet" href="/js/pickadate/lib/themes/classic.time.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.css"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .tab-content{
            border: solid 1px #cccccc;
            border-top: none;
            padding: 15px;
        }

        .nav-tabs > .assigned{
            background: gray;
        }
        .assigned > a{
            color: white !important;
        }

        .nav-tabs > .unresponsive{
            background: red;
        }
        .unresponsive > a{
            color: white !important;
        }

        .nav-tabs > .open1{
            background-color: yellow !important;
        }
        .open > a{
            color: black !important;
        }
        .nav-tabs > .quote{
            background: green;
        }
        .quote > a{
            color: white !important;
        }
        .nav-tabs > .closed{
            background: fuchsia;
        }
        .closed > a{
            color: white !important;
        }
        .nav-tabs > .completed{
            background: purple;
        }
        .completed > a{
            color: white !important;
        }
        .nav-tabs > .discarded{
            background: brown;
        }
        .discarded > a{
            color: white !important;
        }
        .nav-tabs > .lost{
            background: black;
        }
        .lost > a{
            color: white !important;
        }
        .nav-tabs > .postponed{
            background: orange;
        }
        .lost > a{
            color: white !important;
        }
    </style>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">

        @if ( Session::has('message') )
        <div class="alert alert-{{ Session::get('message')['type'] }}" style="margin-bottom: 0">
            {{ Session::get('message')['message'] }}
        </div>
        @endif

        @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin-bottom: 0">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        Elite | Marketing Platform
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Acceso de Usuarios</a></li>
                            {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                        @else
                            <li><a href="{{ action('LeadsController@create') }}"><i class="fa fa-plus"></i> Crear Oportunidad</a></li>
                            @if(Auth::user()->isAdmin())
                            <li><a href="{{ action('LeadsController@allocate') }}"><i class="fa fa-list-ol"></i> Asignación de Contactos</a></li>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                    <i class="fa fa-gear"></i> Administrar <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ action('LeadsController@report') }}"><i class="fa fa-list-ol"></i> Reporte Consolidado</a></li>
                                    <li><a href="{{ action('AdminController@transfer_view') }}"><i class="fa fa-exchange"></i> Transferir Contactos</a></li>
                                    <li><a href="{{ action('AdminController@switch_user_view') }}"><i class="fa fa-suitcase"></i> Cambiar Usuario Activo</a></li>
                                    <li><a href="{{ action('AdminController@users') }}"><i class="fa fa-users"></i> Usuarios</a></li>
                                </ul>
                            </li>
                            @endif
                            <li>
                                <a href="{{ action('LeadsController@assignee') }}">
                                    <i class="fa fa-search"></i> Consulta de Asignados
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

        <div class="container">
            <div class="alert alert-warning">
                <p class="text-center"> <i class="fa fa-question-circle"></i> <b>¿Tienes dudas?</b> <a href="{{ action('LeadsController@tutorial') }}">Consulta ahora mismo nuestra Guía Básica CRM!</a></p>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted text-center">Elite Cirugía Plástica &copy; Todos los derechos reservados {{ date('Y') }}. Desarrollado por
                    <a href="https://www.closerdesign.net">Closer Design Networks</a>.</p>
            </div>
        </footer>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/js/pickadate/lib/picker.js"></script>
    <script src="/js/pickadate/lib/picker.date.js"></script>
    <script src="/js/pickadate/lib/picker.time.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.js"></script>

    <script>
        $(function() {
            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').tab('show');
            }
        });

        $(document).ready(function(){

            $('.datepicker').pickadate({
                format: 'yyyy-mm-dd',
                selectYears: true,
                selectMonths: true
            });

            $('.timepicker').pickatime({
                format: 'HH:i'
            });

            $('.datatable').DataTable();

        });
    </script>

    @yield('js')

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5H4GMDS');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title>{!! Meta::get('title') !!}</title>

    {!! Meta::tag('robots') !!}

    {!! Meta::tag('site_name', 'Elite Cirugía Plástica Colombia') !!}
    {!! Meta::tag('url', Request::url()) !!}

    @if(App::getLocale() == 'en')
    {!! Meta::tag('locale', 'es_ES') !!}
    @elseif(App::getLocale() == 'fr')
    {!! Meta::tag('locale', 'fr_FR') !!}
    @else
    {!! Meta::tag('locale', 'en_EN') !!}
    @endif


    {!! Meta::tag('title') !!}
    {!! Meta::tag('description') !!}

    {{-- Print custom section images and a default image after that --}}
    {!! Meta::tag('image', asset('img/share.jpg')) !!}

    <link rel="stylesheet" href="/all.css">

</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5H4GMDS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- PAGE HEADER [default] -->
<header class="page-header page-header_default">

    <div class="lang-box">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <i class="fa fa-globe"></i> @lang('general.select-language')
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item text-center"><a href="/en">@lang('general.english')</a></li>
                                <li class="list-group-item text-center"><a href="/es">@lang('general.spanish')</a></li>
                                <li class="list-group-item text-center"><a href="/fr">@lang('general.french')</a></li>
                            </ul>
                            <p class="text-center">
                                <a href="#" class="lang-selection-close"><i class="fa fa-close"></i> @lang('general.close')</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="language-bar visible-xs visible-sm">
        <a href="#" class="lang_selection">
            <i class="fa fa-globe"></i> @lang('general.select-language')
        </a>
    </div>

    <div id="whatsapp-cta">
        @lang('front.whatsapp-cta') <a target="_blank" href="https://wa.me/573152251608/?text=Quiero%20asesoria%20personalizada"> <i class="fa fa-whatsapp"></i> @lang('front.whatsapp-btn')</a>
    </div>

    <div class="container">
        <nav class="menu-top">
            <div class="wrapper-logo">
                <a class="logo" href="/">
                    <img src="/img/logo-elite.png" alt="Elite Logo">
                </a>
            </div>
            <div class="wrapper-menu">
                <a class="menu-responsive-button btn btn-default visible-xs" href="#">
                    <i class="fa fa-align-justify"></i>
                </a>

                <a class="close-responsive-button btn btn-default" href="#">
                    <i class="fa fa-close"></i>
                </a>

                @if( URL::to('') === 'https://elitecirugiaplastica.org' )

                <ul class="list-menu">
                    <li class="list-menu__item"><a class="list-menu__link" href="/">@lang('general.home')</a></li>
                    <li class="list-menu__item"><a class="list-menu__link" href="{{ action('PagesController@prices') }}">@lang('general.prices')</a></li>
                    <li class="list-menu__item"><a class="list-menu__link" href="{{ action('PagesController@before_after') }}">@lang('general.before-after')</a></li>
                    <li class="list-menu__item"><a class="list-menu__link" href="{{ action('PagesController@about') }}">@lang('general.about')</a></li>
                    @if(\Illuminate\Support\Facades\App::getLocale() == 'es')
                        <li class="list-menu__item"><a class="list-menu__link" href="{{ action('PagesController@blog') }}">Blog</a></li>
                    @endif
                    <li class="list-menu__item"><a class="list-menu__link" href="{{ action('PagesController@contact') }}">@lang('general.contact')</a></li>
                    <li class="list-menu__item">
                        <a class="list-menu__link lang_selection">
                            <i class="fa fa-globe"></i>
                        </a>
                    </li>
                </ul>

                @endif

            </div>
        </nav>
    </div>
</header>
<!-- - PAGE HEADER [default] -->

@if ( Session::has('message') )
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <div class="alert alert-{{ Session::get('message')['type'] }}">
                    {{ Session::get('message')['message'] }}
                </div>
            </div>
        </div>
    </div>
@endif

@if(isset($nav)) @include('partials._subnav_sites') @else @include('partials._subnav') @endif

@yield('content')

<!-- section about -->
<section class="page-section page-section-about">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <form action="{{ action('FormsController@leads') }}" method="post" class="contact-form contact-home">
                    {{ csrf_field() }}
                    <input type="hidden" name="language" value="{{ App::getLocale() }}">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="h3">@lang('general.instant-advisory')</h3>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" class="form-control" placeholder="@lang('general.name')" name="name" value="{{ old('name') }}" required >
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="email" class="form-control" placeholder="@lang('general.email')" name="email" value="{{ old('email') }}" required >
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="number" class="form-control" placeholder="@lang('general.mobile')" name="phone" value="{{ old('phone') }}" required >
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" class="form-control" placeholder="@lang('general.country')" name="country" value="{{ old('country') }}" required >
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea name="comments" id="comments" cols="30" rows="3"
                                      class="form-control" placeholder="@lang('general.comments')">{{ old('comments') }}</textarea>
                        </div>
                        <div class="col-md-12 checkbox">
                            <label>
                                <input type="checkbox" name="whatsapp" value="1" @if( old('whatspp') == 1 ) checked @endif > <b>@lang('general.whatsapp')</b>
                            </label>
                        </div>
                        <div class="col-md-12 form-group">
                            <button class="btn btn-primary btn-lg" type="submit">@lang('general.ask-for-information')</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="fb-page contact-home" data-href="https://www.facebook.com/EliteCirugiaPlastica/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/EliteCirugiaPlastica/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/EliteCirugiaPlastica/">Elite Cirugía Plástica</a></blockquote></div>
            </div>
        </div>
    </div>
</section>
<!-- - section about -->

<!-- PAGE FOOTER [default] -->
<footer class="page-footer page-footer_default">
    <div class="page-footer__inner-copyrights">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12"><small class="text-copyright">© {{ date('Y') }}. Elite Cirugía Plástica. Una estrategia
                        <a href="https://closerdesign.net" target="_blank">Closer Design Networks</a></small></div>
            </div>
        </div>
    </div>
</footer>
<!-- - PAGE FOOTER [default] -->

<!-- JS Links-->
<script src="/js/modernizr.custom.js"></script>
<script src="/js/jquery.min.js"></script>
<script src="/js/imagesloaded.pkgd.min.js"></script>
<script src="/js/masonry.pkgd.min.js"></script>
<script src="/js/jarallax.min.js"></script>
<script src="/js/jquery.youtubebackground.js"></script>
<script src="/js/jquery.equalheights.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/particles.min.js"></script>
<!-- Custom JS-->
<script src="/js/common.js"></script>

@if ( Session::has('conversion') )
{!! Session::get('conversion') !!}
@endif

@yield('js')

</body>
</html>
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Cirugía Plástica</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app">

    @if ( Session::has('message') )
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-{{ Session::get('message')['type'] }}">
                        {{ Session::get('message')['message'] }}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @yield('content')

    <footer class="footer">
        <div class="container">
            <p class="text-muted text-center">Clínica Cataño y Marquez &copy; Todos los derechos reservados {{ date('Y') }}. Desarrollado por
                <a href="https://www.closerdesign.net">Closer Design Networks</a>.</p>
        </div>
    </footer>

</div>

@yield('js')

</body>
</html>

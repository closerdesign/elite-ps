@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('LeadsController@search') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Búsqueda de Leads</label>
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Ingrese nombre, email o teléfono" >
                    <span class="input-group-btn">
                                <button class="btn btn-success">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
            </div>
        </form>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-briefcase"></i> Mis Oportunidades
            </div>
            <div class="panel-body">

                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" @if($status == null) class="active" @else class="assigned" @endif><a href="/home" ><i class="fa fa-list"></i> Asignados <span class="badge">{{ \App\Lead::pending()->where('user_id', Auth::user()->id)->count() }}</span></a></li>
                        <li role="presentation" @if($status == 'unresponsive') class="active" @else class="unresponsive" @endif><a href="/home/unresponsive"><i class="fa fa-clock-o"></i> No Responden <span class="badge">{{ \App\Lead::unresponsive()->where('user_id', Auth::user()->id)->count() }}</a></li>
                        <li role="presentation" @if($status == 'open') class="active" @else class="open1" @endif><a href="/home/open"><i class="fa fa-cogs"></i> En Proceso <span class="badge">{{ \App\Lead::open()->where('user_id', Auth::user()->id)->count() }}</a></li>
                        <li role="presentation" @if($status == 'quote') class="active" @else class="quote" @endif><a href="/home/quote"><i class="fa fa-credit-card"></i> Cotizados <span class="badge">{{ \App\Lead::quote()->where('user_id', Auth::user()->id)->count() }}</a></li>
                        <li role="presentation" @if($status == 'closed') class="active" @else class="closed" @endif><a href="/home/closed"><i class="fa fa-trophy"></i> Cerrado Ganado <span class="badge">{{ \App\Lead::closed()->where('user_id', Auth::user()->id)->count() }}</a></li>
                        <li role="presentation" @if($status == 'completed') class="active" @else class="completed" @endif><a href="/home/completed"><i class="fa fa-battery-full"></i> Completado <span class="badge">{{ \App\Lead::completed()->where('user_id', Auth::user()->id)->count() }}</a></li>
                        <li role="presentation" @if($status == 'discarded') class="active" @else class="discarded" @endif><a href="/home/discarded"><i class="fa fa-calendar-times-o"></i> Descartados <span class="badge">{{ \App\Lead::discarded()->where('user_id', Auth::user()->id)->count() }}</a></li>
                        <li role="presentation" @if($status == 'lost') class="active" @else class="lost" @endif><a href="/home/lost"><i class="fa fa-frown-o"></i> Pérdidos <span class="badge">{{ \App\Lead::lost()->where('user_id', Auth::user()->id)->count() }}</a></li>
                        <li role="presentation" @if($status == 'postponed') class="active" @else class="postponed" @endif><a href="/home/postponed"><i class="fa fa-clock-o"></i> Pospuestos <span class="badge">{{ \App\Lead::postponed()->where('user_id', Auth::user()->id)->count() }}</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active">
                            <div class="table-responsive">
                                <table class="table table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Email</th>
                                        <th>&nbsp;</th>
                                        <th>Teléfono</th>
                                        <th>País</th>
                                        <th>Creado</th>
                                        <th>Origen</th>
                                        <th>Asignado</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($leads as $lead)
                                        <tr>
                                            <th>
                                                <a href="{{ action('LeadsController@show', $lead->id) }}">
                                                    <i class="fa fa-plus-circle"></i> {{ $lead->name }}
                                                </a>
                                            </th>
                                            <td>{{ $lead->email }}</td>
                                            <td class="text-center">@if($lead->whatsapp == 'Si') <i style="color: green" class="fa fa-whatsapp"></i> @endif</td>
                                            <td>{{ $lead->phone }}</td>
                                            <td>{{ $lead->country }}</td>
                                            <td>{{ date('Y-m-d h:i A', strtotime($lead->created_at)) }}</td>
                                            <td>{{ $lead->source }}</td>
                                            <td class="text-center">{{ $lead->assigned_at }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $leads->render() }}
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-calendar-check-o"></i> Mis Tareas Pendientes
                    </div>
                    <div class="panel panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th colspan="2">Título</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Comentarios</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $status = ['VENCIDA' => 'danger', 'PENDIENTE' => 'warning'] ?>

                                @foreach($tasks as $task)
                                    <tr>
                                        <td><span class="label label-{{ $status[$task->status] }}">{{ $task->status }}</span></td>
                                        <td><a href="{{ action('LeadsController@show', $task->lead->id) }}">[{{ $task->title }}] <b>{{ $task->lead->name }}</b></a></td>
                                        <td><span class="label label-default">{{ $task->type }}</span></td>
                                        <td>{{ date('M d, Y') }}</td>
                                        <td>{{ date('h:i A', strtotime($task->time)) }}</td>
                                        <td>{{ $task->comments }}</td>
                                        <td class="text-center">
                                            <a href="{{ action('LeadsController@show', $task->lead->id) }}">
                                                <i class="fa fa-plus-circle"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $tasks->render() }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-trophy"></i> Mi Rendimiento
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">
                                <tr>
                                    <th>Contactos Asignados</th>
                                    <td class="text-right">{{ \App\Lead::pending()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>No Responden</th>
                                    <td class="text-right">{{ \App\Lead::unresponsive()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>En Proceso</th>
                                    <td class="text-right">{{ \App\Lead::open()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>Cotizados</th>
                                    <td class="text-right">{{ \App\Lead::quote()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>TOTAL EN PROCESO</th>
                                    <td class="text-right">{{ App\Lead::whereIn('status', ['OPEN', 'PENDING', 'UNRESPONSIVE'])->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                @if( \App\Lead::quote()->where('user_id', Auth::user()->id)->count() != 0 )
                                <tr>
                                    <th>% Cotizados</th>
                                    <td class="text-right">{{ round((\App\Lead::quote()->where('user_id', Auth::user()->id)->count() / \App\Lead::whereIn('status', ['OPEN', 'PENDING', 'UNRESPONSIVE'])->where('user_id', Auth::user()->id)->count()) * 100, 2) }}%</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Ganados</th>
                                    <td class="text-right">{{ \App\Lead::closed()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>Completados</th>
                                    <td class="text-right">{{ \App\Lead::completed()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>Descartados</th>
                                    <td class="text-right">{{ \App\Lead::discarded()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>Pérdidos</th>
                                    <td class="text-right">{{ \App\Lead::lost()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                                <tr>
                                    <th>Pospuestos</th>
                                    <td class="text-right">{{ \App\Lead::postponed()->where('user_id', Auth::user()->id)->count() }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

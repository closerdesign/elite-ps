@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                {{ $user->name }}
            </div>
            <div class="panel-body">
                <form action="{{ action('AdminController@user_update', $user->id) }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" name="name" value="{{ $user->name }}" required >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ $user->email }}" required >
                    </div>
                    <div class="form-group">
                        <label for="is_admin">Es Administrador?</label>
                        <select name="is_admin" id="is_admin" class="form-control" required >
                            <option value="">Seleccionar...</option>
                            <option @if( $user->is_admin == 'Si' ) selected @endif value="1">Si</option>
                            <option @if( $user->is_admin == 'No' ) selected @endif value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="is_assigner">Es Asignador?</label>
                        <select name="is_assigner" id="is_assigner" class="form-control" required >
                            <option value="">Seleccionar...</option>
                            <option @if( $user->is_assigner == 'Si' ) selected @endif value="1">Si</option>
                            <option @if( $user->is_assigner == 'No' ) selected @endif value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="team">Equipo</label>
                        <select name="team" id="team" class="form-control">
                            <option value="">Seleccione...</option>
                            <option @if( $user->team == 'BOG' ) selected @endif value="BOG">Bogotá</option>
                            <option @if( $user->team == 'CTG' ) selected @endif value="CTG">Cartagena</option>
                        </select>
                    </div>
                    <button class="btn btn-success">
                        <i class="fa fa-save"></i> Guardar Cambios
                    </button>
                </form>
            </div>
        </div>
        <form
                action="{{ action('AdminController@user_delete', $user->id) }}"
                method="post"
                onsubmit="return confirm('Estás seguro? Esta operación no podrá deshacerse.')"
                class="form-group"
        >
            {{ csrf_field() }}
            <button class="btn btn-danger form-control">
                <i class="fa fa-trash"></i> Eliminar Usuario
            </button>
        </form>
    </div>

    @endsection
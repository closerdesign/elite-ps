@extends('layouts.app')

@section('content')

    <form action="{{ action('AdminController@transfer') }}" method="post" onsubmit="return confirm('Esta seguro? Esta operación no podrá deshacerse.')">
        {{ csrf_field() }}
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Transferir Contactos
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="from">Asesor Origen</label>
                        <select name="from" id="from" class="form-control" required >
                            <option value="">Seleccionar...</option>
                            @foreach(\App\User::orderBy('name')->get() as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="to">Asesor Destino</label>
                        <select name="to" id="to" class="form-control" required >
                            <option value="">Seleccionar...</option>
                            @foreach(\App\User::orderBy('name')->get() as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-success">
                        <i class="fa fa-gear"></i> Procesar
                    </button>
                </div>
            </div>
        </div>
    </form>

    @endsection
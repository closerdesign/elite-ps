@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-users"></i> Usuarios
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Administrador</th>
                            <th>Asignador</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr>
                            <th>
                                <a href="{{ action('AdminController@user_details', $user->id) }}">
                                    {{ $user->name }}
                                </a>
                            </th>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->is_admin }}</td>
                            <td>{{ $user->is_assigner }}</td>
                            <td class="text-center">
                                <a href="{{ action('AdminController@user_details', $user->id) }}">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection
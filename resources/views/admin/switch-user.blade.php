@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Cambiar Usuario Activo
            </div>
            <div class="panel-body">
                <div class="alert alert-info">
                    Importante: Si el usuario por el que vas a intercambiar sesión no es un usuario administrador, deberás cerrar sessión y volver a ingresar para volver a tu cuenta de usuario.
                </div>
                <ul class="list-group">
                    @foreach(\App\User::orderBy('name')->get() as $user)
                    <li class="list-group-item">
                        <a href="{{ action('AdminController@switch_user', $user->id) }}">
                            <i class="fa fa-arrow-circle-right"></i> {{ $user->name }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    @endsection
@if( (URL::to('') === 'https://elitecirugiaplastica.org') || URL::to('') === 'http://elite-ps.test:8000' )
<section class="elite-menu">
    <div class="container">
        <div class="row elite-menu-content">
            <a href="#" class="col-md-2 col-sm-3 col-xs-12 btn-home">@lang('general.click-here') <i class="fa fa-caret-down"></i></a>
            <a href="/" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.medical-team')</a>
            <a href="{{ action('PagesController@testimonials') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.testimonials')</a>
            <a href="{{ action('PagesController@before_after') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.before-after')</a>
            <a href="{{ action('PagesController@prices') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.prices')</a>
            <a href="{{ action('PagesController@colombia') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">Colombia</a>
            <a href="{{ action('PagesController@contact') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.contact')</a>
            <a href="{{ action('PagesController@about') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.about')</a>
            <a href="{{ action('PagesController@why_elite') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.why-elite')</a>
            <a href="{{ action('PagesController@security') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.your-safety')</a>
            <a href="{{ action('PagesController@arrival') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.reception')</a>
            <a href="{{ action('PagesController@hosting') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.accomodation')</a>
            <a href="{{ action('PagesController@advisors') }}" class="col-md-2 col-sm-3 col-xs-12 home-option">@lang('general.advisory-team')</a>
            <a href="{{ action('PagesController@blog') }}" class="col-md-2 col-sm-3 col-xs-12 hidden-md hidden-lg hidden-sm home-option">Blog</a>
        </div>
    </div>
</section>
@endif
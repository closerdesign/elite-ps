<header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse text-right">@lang('general.instant-advisory')</span>
    <hr class="divider">
    <div class="asesoria-inmediata video-home">
        @if(!isset($nav))
        <p style="background: #07E26B">
            <a
                    style="color: #ffffff;
                    font-size: 12px;"
                    href="https://wa.me/573152251608/?text=Quiero%20asesoria%20personalizada">
                <b><i class="fa fa-whatsapp"></i> @lang('front.whatsapp-cta')<br /><span style="padding: 3px; border: solid 1px #ffffff; border-radius: 2px">@lang('front.whatsapp-btn')</span></b>
            </a>
        </p>
        <h3>
            @lang('advisory.call-us-now')
        </h3>
        <p><b>@lang('advisory.usa'): <a href="tel:305-407-9404">305-407-9404</a></b></p>
        <p><b>@lang('advisory.spain'): <a href="tel:911-875-660">911-875-660</a></b></p>
        <p><b>@lang('advisory.puerto-rico'): <a href="tel:787-338-5414">787-338-5414</a></b></p>
        <p><b>@lang('advisory.canada'): <a href="tel:519-914-1648">519-914-1648</a></b></p>
        <p><b>@lang('advisory.australia'): <a href="tel:61-283-104-287">61-283-104-287</a></b></p>
        <p><b>@lang('advisory.panama'): <a href="tel:519-914-1648">507-832-2426</a></b></p>
        <p><b>@lang('advisory.costa-rica'): <a href="tel:506-4001-0376">506-4001-0376</a></b></p>
        <p><b>@lang('advisory.chile'): <a href="tel:562-240-53500">562-240-53500</a></b></p>
        @else
        <h3 class="text-center">
            @lang('advisory.call-us-now')
        </h3>
        <p class="text-center">
            <b>
                <img width="20" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAADpElEQVQ4T5WUf0yUdRzH35/vwXHCCZ60lAHya1K0E1vZQPoxhKTNKV3hAf6IACOrzUxSpkJ0WbHKmE6qTYWrRhkIQfWfHU1zOddyKi2XI1TkCI6VIBxw3nPP8/225wp2z53W+v71PJ8fr+/nx/fzIdzmlJ8sN3hkViyIWYQQK8D5YtWMERuGwDnoRDcfc3d0FHdIwe4ULCjpqdwkBN9P0MXd7rJZmRB8iMCq2wtaOgLt5oDW41YdM83/CKDn/w0UquMfZJxJ2m6z2biqmwOWOCoP/3/YP3hOTe1PNL88Byx1bNkogM9D60FINMYjaX4CuJfDKQ3DOfU7BERIoAJi/fHV9i9JbcCMjKvBNYuPjENV+jPweDy42N8L7uNYvjQT0TExaO5rxeDUkAbKAafJpEujEkdlGUCfBmrv5rHY9cA2vNn2Di5e7dU4LksxY19ZPZouH8GA26nREVEpFfc810VCPDWr8Y7fwlvZtTjQfQgjYy5kppjxXe8pjeN9SzJQ/+xe1J5/G0Jo0m8nq2PLIAMSVY9b4x4sGIlC1eoK7Dhag5qiahRmrcUamwWTM5MaaOML78Ix8T36Jq4EyHk/WU9USIyxcBU28tMQCleswUKjCR/3tKIsbxOKcix4uqEUClc0wM2rNiAqLRqO4cDo+YwfKE14w1WYkDks2etgMi7wA/VhenzyyhF0nf0anWe6cX9KJi4N/gqf4sPmvA2IStUCOTBNhceKna4LrgQVph61PlUFFdjRXOP/vys6Fu9VNEAfFo70+KU4fekH7GzZgwMv7seJmyeDUsZvtHJ37mnZKz86mw8RobW6BXWfvYGB0et+MSOGrHsewrJkM74934NIQyReK9uDugsNmqYQxBf0SH3+q1639/1AYOfuYyg/WIWI8Aj8OXlD+2ySzXh941582G/H9amQZ2OlB23rIsk97iSOhaqnGkXT1kaMTY1DURS4bo7i52u/ABDITDbDOM+Io32tcOGPoGlRro2GJaf7ZzmnLm+bb1o6pH4/nLESBr0BZy//CI/kQVpcKu6NT/cPW7/rCqaXSNDHRISMHgnxZFuB/Zu55ZBd+1inMsOL7rRpWBhDXFYiImIMoSYkGtsft+/UbBur1aobSr3RJkvS+mCP/4Tl23eB/t4YIQs2x5b/ktct7WNcxPo7fIfIOGFAx8V2Nc3AAEKAqjLXlmuQuW6r7FMsi5YvTjLEzlvEwQQDhgniHIh1uXQJX51aZZODs/kL5IZwg4VYTJwAAAAASUVORK5CYII=" alt="">
                <a href="tel:+57(318)5893069">+57(318)5893069</a>
            </b>
        </p>
        @endif
    </div>
</header>
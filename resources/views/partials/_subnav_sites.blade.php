<section class="elite-menu">
    <div class="container">
        <div class="row">
            <a href="#" class="col-md-2 col-sm-3 col-xs-12 btn-home">@lang('general.click-here') <i class="fa fa-caret-down"></i></a>
            <a href="{{ action('SitesController@before_after') }}" class="col-md-4 col-sm-3 col-xs-12 home-option">@lang('general.before-after')</a>
            <a href="{{ action('SitesController@index') }}" class="col-md-4 col-sm-3 col-xs-12 home-option">@lang('general.prices')</a>
            <a href="{{ action('SitesController@contact') }}" class="col-md-4 col-sm-3 col-xs-12 home-option">@lang('general.contact')</a>
        </div>
    </div>
</section>
@extends('layouts.sites')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-md-3">
                <img src="/images/logo-cym.jpg" alt="" class="img-responsive">
            </div>
        </div>
    </div>

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro [video background] -->
        <section class="page-section page-section_intro _overlay-pattern">
            <div class="page-section_intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Precios y Paquetes
                                </div>
                                <div class="panel-body">
                                    {{--<header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">Precios y Paquetes</span>--}}
                                        {{--<hr class="divider">--}}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="paquetes video-home">
                                                    <p><img class="img-responsive" src="/img/paquete-3-replaced.jpg" alt=""></p>
                                                    <h2>Paquete # 3</h2>
                                                    <p>(3) Cirugías deseadas DESDE $12,900,000</p>
                                                    <ul>
                                                        <li>Lipoescultura Láser Completa (brazos, tronco y piernas)</li>
                                                        <li>Mamoplastia (aumento, levantamiento ó reducción)</li>
                                                        <li>Abdominoplastia</li>
                                                        <li>Obsequio gratís: Aumento de gluteos con grasa propia..</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="paquetes video-home">
                                                    <p><img class="img-responsive" src="/img/paquete-2-replaced.jpg" alt=""></p>
                                                    <h2>Paquete # 2</h2>
                                                    <p>(2) Cirugías deseadas DESDE $8,900,000</p>
                                                    <ul>
                                                        <li>Lipoescultura Láser Completa (brazos, tronco y piernas)</li>
                                                        <li>Abdominoplastia ó Cirugia de Senos</li>
                                                        <li>Obsequio gratís: Aumento de gluteos con grasa propia.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="paquetes video-home">
                                                    <p><img class="img-responsive" src="/img/paquete-1-replaced.jpg" alt=""></p>
                                                    <h2>Paquete # 1</h2>
                                                    <p>(1) Cirugías deseadas DESDE $5,900,000</p>
                                                    <ul>
                                                        <li>Lipoescultura Láser Completa (brazos, tronco y piernas)</li>
                                                        <li>Obsequio gratís: Aumento de gluteos con grasa propia.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </header>
                                </div>
                            </div>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    @include('partials._advisory')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section intro [video background] -->

        <div style="background: #f5f5f5;">
            <!-- section about -->
            <section class="page-section page-section-about">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <h2 class="h2 text-center">Otros Paquetes</h2>
                            <ul class="list-group">
                                <li class="list-group-item">Rinoplastia: $4,500,000</li>
                                <li class="list-group-item">Mamoplastia de Aumento: $5,800,000</li>
                                <li class="list-group-item">Mamoplastia de Reducción y Levantamiento: $5,900,000</li>
                                <li class="list-group-item">Cirugía de Párpados: $4,200,000</li>
                                <li class="list-group-item">Cirugía de Orejas: $4,000,000</li>
                                <li class="list-group-item">Cirugía de Cachetes (Bichectomía): $2,800,000</li>
                                <li class="list-group-item">Cirugía de Gluteos con Implante: $7,900,000</li>
                                <li class="list-group-item">Lifting Cérvico Facial: $9,000,000</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- - section about -->
        </div>

        <!-- section about -->
        <section class="page-section page-section-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="h2 text-center">¿Qué incluye?</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <ul class="list-group">
                            <li class="list-group-item">Consulta Cirujano(a) Certificado.</li>
                            <li class="list-group-item">Consulta Pre anestésica.</li>
                            <li class="list-group-item">Póliza de Seguro Médico (Cobertura Cirugía Plástica).</li>
                            <li class="list-group-item">Procedimientos Quirúrgicos.</li>
                            <li class="list-group-item">Honorarios Cirujanos Plásticos y/o especialistas.</li>
                            <li class="list-group-item">Honorarios Anestesiólogo.</li>
                            <li class="list-group-item">Clínica / Sala de Cirugía.</li>
                            <li class="list-group-item">10 Masajes A domicilio dentro de Bogotá.</li>
                            <li class="list-group-item">3 Controles Post- Quirúrgicos (Visitas al Medico).</li>
                            <li class="list-group-item">2 Fajas moldeadoras Post Quirúrgica.</li>
                            <li class="list-group-item">2 Sostén o brasier Post Quirúrgico.</li>
                            <li class="list-group-item">Tabla Postquirurgica.</li>
                            <li class="list-group-item">Medicamentos Básicos.</li>
                            <li class="list-group-item">Antibióticos.</li>
                            <li class="list-group-item">Antinflamatorios.</li>
                            <li class="list-group-item">Analgésicos.</li>
                            <li class="list-group-item">Anticoagulantes.</li>
                            <li class="list-group-item">Pañales Post Quirúrgicos.</li>
                            <li class="list-group-item">Medias anti trombos.</li>
                            <li class="list-group-item"><span class="label label-danger">¡ADICIONAL!</span> Servicio de cuidados de enfermería a domicilio durante 10 días.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section about -->

        <div style="background-color: #f5f5f5; border-bottom: solid 1px #9763A2">
            <!-- section about -->
            <section class="page-section page-section-about">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <form action="{{ action('FormsController@leads') }}" method="post" class="contact-form contact-home">
                                {{ csrf_field() }}
                                <input type="hidden" name="language" value="{{ App::getLocale() }}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="h3">@lang('general.instant-advisory')</h3>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" class="form-control" placeholder="@lang('general.name')" name="name" value="{{ old('name') }}" required >
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="email" class="form-control" placeholder="@lang('general.email')" name="email" value="{{ old('email') }}" required >
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="number" class="form-control" placeholder="@lang('general.mobile')" name="phone" value="{{ old('phone') }}" required >
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" class="form-control" placeholder="@lang('general.country')" name="country" value="{{ old('country') }}" required >
                                    </div>
                                    <div class="col-md-12 form-group">
                            <textarea name="comments" id="comments" cols="30" rows="3"
                                      class="form-control" placeholder="@lang('general.comments')">{{ old('comments') }}</textarea>
                                    </div>
                                    <div class="col-md-12 checkbox">
                                        <label>
                                            <input type="checkbox" name="whatsapp" value="1" @if( old('whatspp') == 1 ) checked @endif > <b>@lang('general.whatsapp')</b>
                                        </label>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <button class="btn btn-primary btn-lg" type="submit">@lang('general.ask-for-information')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!-- - section about -->
        </div>

    </main>
    <!-- - PAGE MAIN -->

@endsection
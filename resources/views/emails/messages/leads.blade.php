@extends('emails.layouts.base')

@section('content')

    <p>A continuación la información relacionada:</p>

    <p><b>Nombre</b><br />{{ $lead->name }}</p>
    <p><b>Email</b><br />{{ $lead->email }}</p>
    <p><b>Teléfono</b><br />{{ $lead->phone }}</p>
    <p><b>País</b><br />{{ $lead->country }}</p>
    <p><b>Whatsapp</b><br />{{ $lead->whatsapp }}</p>

    @if($lead->comments != "")
        <p><b>Comentarios</b><br />{{ $lead->comments }}</p>
    @endif

    @endsection
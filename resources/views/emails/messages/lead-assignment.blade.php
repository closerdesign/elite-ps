@extends('emails.layouts.base')

@section('content')

    <h1 style="text-align: center"><b>¡Hola {{ explode(' ', \App\User::findOrFail($lead->user_id)->name)[0] }}!</b></h1>

    <p style="text-align: center">Queremos informarte que un nuevo contacto te ha sido asignado.</p>

    <p style="text-align: center">Por favor haz click en el enlace a continuación para comenzar a gestionarlo:</p>

    <h2 style="text-align: center">
        <a href="{{ action('LeadsController@show', $lead->id) }}">
            Ver asignación:<br />
            <b>
                <span style="text-transform: capitalize">{{ strtolower($lead->name) }}</span>
            </b>
        </a>
    </h2>

    <p style="text-align: center">
        <a target="_blank" href="{{ action('LeadsController@vcard', $lead->id) }}">
            Descargar Contacto
        </a>
    </p>

    <p style="text-align: center"><b>¡Buena pesca!</b> Recuerda que entre más rápido contactes a tus pacientes, mayores son tus posibilidades de ganar.</p>

    @endsection
@extends('emails.layouts.base')

@section('content')

    <p><b>¡Buenos días {{ explode(" ", $user->name)[0] }}!</b></p>

    <p>A continuación encontrarás el listado de tus tareas en estado pendiente.</p>

    @foreach($tasks as $task)
        <table width="100%" border="0" cellpadding="5" bgcolor="#f3f3f3">
            <tr>
                <td bgcolor="#ffffff">
                    <b>
                        <a href="{{ action('LeadsController@show', $task->lead->id) }}">
                            {{ $task->title }} [{{ $task->lead->name }}]
                        </a>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Fecha: </b>
                    @if( date('Y-m-d', strtotime($task->date)) == date('Y-m-d') )
                        HOY
                    @else
                        {{ date('M d, Y', strtotime($task->date)) }}
                    @endif
                    <br />
                    <b>Hora:</b> {{ date('h:i A', strtotime($task->time)) }}<br />
                    <b>Estado:</b> {{ $task->status }}
                </td>
            </tr>
        </table>
        <br>
        @endforeach

    <p style="text-align: center">{{ count($tasks) }} tareas en total.</p>

    @endsection
@extends('emails.layouts.base')

@section('content')

    <p>A continuación la información relacionada:</p>

    @foreach( $user->tasks as $task )
        <hr>
        <p>
            <b>{{ $task->title }}</b><br />
            {{ $task->comments }}<br />
            <a href="{{ action('LeadsController@show', $task->lead_id) }}">
                Paciente: {{ $task->lead->name }}, Caso # {{ $task->lead_id }}<br />
                Haz click aquí para ver el detalle
            </a>
        </p>
    @endforeach

    @endsection
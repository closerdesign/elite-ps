@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if( isset($_GET['advisor']) )
                    <div class="form-group">
                        <a href="{{ action('LeadsController@report') }}" class="btn btn-success">
                            <i class="fa fa-arrow-circle-left"></i>
                        </a>
                    </div>
                @else
                <form action="">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="advisor" id="advisor" class="form-control">
                                <option value="">Seleccione Asesor...</option>
                                @foreach( \App\User::orderBy('name')->get() as $user )
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-btn">
                                <button class="btn btn-success">
                                    Filtrar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                @endif

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-trophy"></i> @if( isset($_GET['advisor']) ) Mi Rendimiento: {{ \App\User::findOrFail($_GET['advisor'])->name }} @else Rendimiento Consolidado @endif
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed">

                                @if( isset($_GET['advisor']) )

                                    <tr>
                                        <th>Contactos Asignados</th>
                                        <td class="text-right">{{ \App\Lead::pending()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>No Responden</th>
                                        <td class="text-right">{{ \App\Lead::unresponsive()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>En Proceso</th>
                                        <td class="text-right">{{ \App\Lead::open()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cotizados</th>
                                        <td class="text-right">{{ \App\Lead::quote()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>TOTAL EN PROGRESO</th>
                                        <td class="text-right">{{ App\Lead::whereIn('status', ['OPEN', 'PENDING', 'UNRESPONSIVE'])->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    @if( \App\Lead::quote()->where('user_id', $_GET['advisor'])->count() != 0 )
                                        <tr>
                                            <th>% Cotizados</th>
                                            <td class="text-right">{{ round((\App\Lead::quote()->where('user_id', $_GET['advisor'])->count() / \App\Lead::whereIn('status', ['OPEN', 'PENDING', 'UNRESPONSIVE'])->where('user_id', $_GET['advisor'])->count()) * 100, 2) }}%</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th>Ganados</th>
                                        <td class="text-right">{{ \App\Lead::closed()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Completados</th>
                                        <td class="text-right">{{ \App\Lead::completed()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Descartados</th>
                                        <td class="text-right">{{ \App\Lead::discarded()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pérdidos</th>
                                        <td class="text-right">{{ \App\Lead::lost()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pospuestos</th>
                                        <td class="text-right">{{ \App\Lead::postponed()->where('user_id', $_GET['advisor'])->count() }}</td>
                                    </tr>

                                @else

                                    <tr>
                                        <th>Contactos Asignados</th>
                                        <td class="text-right">{{ \App\Lead::pending()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>No Responden</th>
                                        <td class="text-right">{{ \App\Lead::unresponsive()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>En Proceso</th>
                                        <td class="text-right">{{ \App\Lead::open()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Cotizados</th>
                                        <td class="text-right">{{ \App\Lead::quote()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>TOTAL EN PROGRESO</th>
                                        <td class="text-right">{{ App\Lead::inProgress()->count() }}</td>
                                    </tr>
                                    @if( \App\Lead::quote()->count() != 0 )
                                        <tr>
                                            <th>% Cotizados</th>
                                            <td class="text-right">{{ round((\App\Lead::quote()->count() / \App\Lead::whereIn('status', ['OPEN', 'PENDING', 'UNRESPONSIVE'])->count()) * 100, 2) }}%</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th>Ganados</th>
                                        <td class="text-right">{{ \App\Lead::closed()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Completados</th>
                                        <td class="text-right">{{ \App\Lead::completed()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Descartados</th>
                                        <td class="text-right">{{ \App\Lead::discarded()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pérdidos</th>
                                        <td class="text-right">{{ \App\Lead::lost()->count() }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pospuestos</th>
                                        <td class="text-right">{{ \App\Lead::postponed()->count() }}</td>
                                    </tr>

                                @endif

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
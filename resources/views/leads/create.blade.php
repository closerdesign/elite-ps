@extends('layouts.app')

@section('content')

    <div class="container">
        <form action="{{ action('LeadsController@store') }}" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fa fa-user"></i> Crear Una Nueva Oportunidad</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" >
                    </div>
                    <div class="form-group">
                        <label for="phone">Teléfono</label>
                        <input type="number" class="form-control" name="phone" value="{{ old('phone') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="country">País</label>
                        <input type="text" class="form-control" name="country" value="{{ old('country') }}" required >
                    </div>
                    <div class="form-group">
                        <label for="comments">Comentarios</label>
                        <textarea name="comments" id="comments" cols="30" rows="10" class="form-control">{{ old('comments') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="whatsapp">¿Usa WhatsApp?</label>
                        <select name="whatsapp" id="whatsapp" class="form-control">
                            <option value="">Seleccione...</option>
                            <option @if( old('whatsapp') == '1' ) selected @endif value="1">Si</option>
                            <option @if( old('whatsapp') == '0' ) selected @endif value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="language">Idioma</label>
                        <select name="language" id="language" class="form-control" required >
                            <option value="">Seleccione...</option>
                            <option value="en">English</option>
                            <option value="es">Español</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="source">Origen</label>
                        <select name="source" id="source" class="form-control">
                            <option value="">Seleccione...</option>
                            <option value="FACEBOOK">Facebook</option>
                            <option value="INSTAGRAM">Instagtam</option>
                            <option value="WHATSAPP">WhatsApp</option>
                            <option value="ZOPIM">Zopim</option>
                            <option value="EMAIL">Email</option>
                            <option value="PHONE">Teléfono</option>
                            <option value="REFERRER">Referido</option>
                        </select>
                    </div>
                    @if(Auth::user()->isAdmin() && Auth::user()->isAssigner())
                        <div class="form-group">
                            <label for="user_id">Asesor</label>
                            <select name="user_id" id="user_id" class="form-control" >
                                <option value="">Seleccione...</option>
                                @foreach(\App\User::orderBy('name')->get() as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    @endif
                </div>
            </div>
            <button class="btn btn-success pull-right">
                <i class="fa fa-save"></i> Guardar Cambios
            </button>
        </form>
    </div>

    @endsection
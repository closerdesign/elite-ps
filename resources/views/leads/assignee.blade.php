@extends('layouts.app')

@section('content')

    <div class="container">

        @if( !isset($_GET['keyword']) )

            <form action="">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Consulta de Asignados
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" name="keyword" placeholder="Ingrese Nombre, Teléfono ó Correo Electrónico" required >
                                <span class="input-group-btn">
                                    <button class="btn btn-success">
                                        <i class="fa fa-search"></i> Consultar
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        @else

            <div class="form-group">
                <a href="{{ action('LeadsController@assignee') }}" class="btn btn-success">
                    <i class="fa fa-arrow-circle-left"></i>
                </a>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    Consulta de Asignados: {{ $_GET['keyword'] }}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed">
                            <tr>
                                <th>Nombre Paciente</th>
                                <th>Nombre Asesor</th>
                                <th>Estado</th>
                                <th>Asginado en</th>
                            </tr>
                            @foreach( \App\Lead::where('name', 'like', '%' . $_GET['keyword'] . '%')->orWhere('phone', 'like', '%' . $_GET['keyword'] . '%')->orWhere('email', 'like', '%' . $_GET['keyword'] . '%')->get() as $lead )
                            <tr>
                                <td>{{ $lead->name }}</td>
                                <td>@if( $lead->advisor ) {{ $lead->advisor->name }} @else Sin Asignar @endif</td>
                                <td>{{ $lead->status }}</td>
                                <td>{{ $lead->assigned_at }}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

        @endif

    </div>

    @endsection
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="{{ action('LeadsController@search') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="keyword" placeholder="Ingrese nombre, email o teléfono" >
                            <span class="input-group-btn">
                                <button class="btn btn-success">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-briefcase"></i> Resultados para: <b>{{ $keyword }}</b> ({{ count($leads) }} resultados encontrados).
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped datatable">
                                <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th>&nbsp;</th>
                                    <th>Teléfono</th>
                                    <th>País</th>
                                    <th>Creado</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($leads as $lead)
                                    <tr>
                                        <td>
                                            <span class="label label-default">
                                                {{ $lead->status }}
                                            </span>
                                        </td>
                                        <th>
                                            <a href="{{ action('LeadsController@show', $lead->id) }}">
                                                {{ $lead->name }}
                                            </a>
                                        </th>
                                        <td>{{ $lead->email }}</td>
                                        <td class="text-center">@if($lead->whatsapp == 'Si') <i style="color: green" class="fa fa-whatsapp"></i> @endif</td>
                                        <td>{{ $lead->phone }}</td>
                                        <td>{{ $lead->country }}</td>
                                        <td>{{ date('Y-m-d h:i A', strtotime($lead->created_at)) }}</td>
                                        <td class="text-center">
                                            <a href="{{ action('LeadsController@show', $lead->id) }}">
                                                <i class="fa fa-plus-circle"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                Video Tutorial - Guía Básica CRM
            </div>
            <div class="panel-body">
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rMF4PFx7jRc?rel=0"></iframe>
                </div>
            </div>
        </div>
    </div>

    @endsection
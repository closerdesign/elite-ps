@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="panel panel-default">
            <div class="panel-body">
                <p class="lead text-center" style="margin-bottom: 0">
                    <a href="{{ action('LeadsController@vcard', $lead->id) }}">
                        <i class="fa fa-whatsapp"></i> Caso No. {{ $lead->id }} - <span style="text-transform: capitalize">{{ strtolower($lead->name) }}</span>
                    </a>
                </p>
            </div>
        </div>

        @if( $lead->quote != "" )
        <p>
            <a target="_blank" href="{{ $lead->quote }}" class="btn btn-success form-control" ><i class="fa fa-download"></i> Descargar Cotización</a>
        </p>
        @endif

        <form action="{{ action('LeadsController@update', $lead->id) }}" method="post" enctype="multipart/form-data" >
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-group">
                <div class="input-group">
                    <input type="file" class="form-control" name="quote" required >
                    <span class="input-group-btn">
                            <button class="btn btn-success"><i class="fa fa-upload"></i> Cargar Cotización</button>
                        </span>
                </div>
            </div>
        </form>

        <div class="row">

            <div class="col-md-8">

                <div class="panel panel-success">
                    <div class="panel-heading"><i class="fa fa-address-card"></i> Estado</div>
                    <div class="panel-body">
                        <form action="{{ action('LeadsController@update', $lead->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="input-group">
                                <select name="status" id="status" class="form-control">
                                    <option value="">Seleccione...</option>
                                    {{--<option @if($lead->status == 'PENDING') selected @endif value="PENDING">ASIGNADO</option>--}}
                                    <option @if($lead->status == 'OPEN') selected @endif value="OPEN">EN PROCESO (Recibiendo Información)</option>
                                    <option @if($lead->status == 'CLOSED') selected @endif value="CLOSED">CERRADO GANADO</option>
                                    <option @if($lead->status == 'COMPLETED') selected @endif value="COMPLETED">COMPLETADO</option>
                                    <option @if($lead->status == 'POSTPONED') selected @endif value="POSTPONED">POSPUESTO</option>
                                    <option @if($lead->status == 'UNRESPONSIVE') selected @endif value="UNRESPONSIVE">NO RESPONDE</option>
                                    <option @if($lead->status == 'DISCARDED') selected @endif value="DISCARDED">DESCARTADO</option>
                                    <option @if($lead->status == 'LOST') selected @endif value="LOST">PÉRDIDO</option>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-success">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-info-circle"></i> Información Básica</a></li>
                        <li role="presentation"><a href="#details" aria-controls="details" role="tab" data-toggle="tab"><i class="fa fa-plus-circle"></i> Detalles</a></li>
                        <li role="presentation"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab"><i class="fa fa-tasks"></i> Crear Tarea</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">

                            <form action="{{ action('LeadsController@update', $lead->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('PATCH')  }}
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="name">Nombre</label>
                                        <input type="text" class="form-control" name="name" value="{{ $lead->name }}" required >
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" value="{{ $lead->email }}" required >
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="phone">Teléfono</label>
                                        <input type="text" class="form-control" name="phone" value="{{ $lead->phone }}" required >
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="country">País</label>
                                        <input type="text" class="form-control" name="country" value="{{ $lead->country }}" required >
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="comments">Comentarios</label>
                                        <textarea name="comments" id="comments" cols="30" rows="10"
                                                  class="form-control" readonly >{{ $lead->comments }}</textarea>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="whatsapp">WhatsApp</label>
                                        <select name="whatsapp" id="whatsapp" class="form-control" required >
                                            <option value="">Seleccione...</option>
                                            <option @if($lead->whatsapp == 'Si') selected @endif value="1">Si</option>
                                            <option @if($lead->whatsapp == 'No') selected @endif value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="language">Idioma</label>
                                        <select name="language" id="language" class="form-control" required >
                                            <option value="">Seleccione...</option>
                                            <option @if($lead->language == 'Español') selected @endif value="es">Español</option>
                                            <option @if($lead->language == 'English') selected @endif value="en">English</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="source">Origen</label>
                                        <input type="text" class="form-control" name="source" value="{{ $lead->source }}" readonly >
                                    </div>
                                </div>
                                <button class="btn btn-success">
                                    <i class="fa fa-save"></i> Guardar cambios
                                </button>
                            </form>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="details">
                            <form action="{{ action('LeadsController@update', $lead->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Información Complementaria</div>
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label for="advisory_start_date">Fecha de Inicio Asesoría</label>
                                            <input type="text" class="form-control datepicker" name="advisory_start_date" value="{{ $lead->advisory_start_date }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="destination">Destino</label>
                                            <select name="destination" id="destination" class="form-control">
                                                <option value="">Seleccione...</option>
                                                <option @if($lead->destination == 'BOGOTA') selected @endif value="BOGOTA">BOGOTA</option>
                                                <option @if($lead->destination == 'CARTAGENA') selected @endif value="CARTAGENA">CARTAGENA</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="procedures">Procedimientos</label>
                                            <textarea name="procedures" id="procedures" cols="30" rows="10"
                                                      class="form-control">{{ $lead->procedures }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="currency">Moneda</label>
                                            <select name="currency" id="currency" class="form-control">
                                                <option value="">Seleccione...</option>
                                                <option @if($lead->currency == 'USD') selected @endif value="USD">Dólares Americanos</option>
                                                <option @if($lead->currency == 'EUR') selected @endif value="EUR">Euros</option>
                                                <option @if($lead->currency == 'COP') selected @endif value="COP">Pesos Colombianos</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="amount">Valor Cotizado</label>
                                            <input type="number" class="form-control" name="amount" value="{{ $lead->amount }}" >
                                        </div>

                                        <div class="form-group">
                                            <label for="estimated_closing_date">Fecha Estimada de Cierre</label>
                                            <input type="text" class="form-control datepicker" name="estimated_closing_date" value="{{ $lead->estimated_closing_date }}" >
                                        </div>

                                        <div class="form-group">
                                            <label for="additional_comments">Comentarios Adicionales</label>
                                            <textarea name="additional_comments" id="additional_comments" cols="30"
                                                      rows="10"
                                                      class="form-control">{{ $lead->additional_comments }}</textarea>
                                        </div>

                                    </div>
                                </div>
                                <button class="btn btn-success form-control">
                                    <i class="fa fa-save"></i> Guardar Cambios
                                </button>
                            </form>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="tasks">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Crear Nueva Tarea</div>
                                <div class="panel-body">
                                    <form action="{{ action('TasksController@store') }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="lead_id" value="{{ $lead->id }}">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="title" placeholder="Título" required >
                                        </div>
                                        <div class="form-group">
                                            <select name="type" id="type" class="form-control" required >
                                                <option value="">Seleccione tipo...</option>
                                                <option value="CALL">Llamada</option>
                                                <option value="EMAIL">Email</option>
                                                <option value="MEETING">Reunión</option>
                                                <option value="OTHER">Otro</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control datepicker" name="date" placeholder="Fecha" required >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control timepicker" name="time" placeholder="Hora" required >
                                        </div>
                                        <div class="form-group">
                                                            <textarea name="comments" id="comments" cols="30" rows="10"
                                                                      class="form-control" placeholder="Comentarios..."></textarea>
                                        </div>
                                        <button class="btn btn-success form-control">
                                            <i class="fa fa-thumb-tack"></i> Agregar Tarea
                                        </button>
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <div class="col-md-4">

                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-tasks"></i> Tareas Pendientes</div>
                    <div class="panel-body">
                        @foreach($lead->tasks->where('status', '!=', 'COMPLETADA') as $task)
                        <div class="panel panel-default">
                            <?php $status = ['VENCIDA' => 'danger', 'PENDIENTE' => 'warning'] ?>
                            <div class="panel-heading"><span class="label label-{{ $status[$task->status] }}">{{ $task->status }}</span> {{ $task->title }}</div>
                            <div class="panel-body">
                                <p>
                                    <b>Tipo:</b> {{ $task->type }}<br />
                                    <b>Fecha:</b> {{ date('M d, Y', strtotime($task->date)) }}<br />
                                    <b>Hora:</b> {{ date('h:i A', strtotime($task->time)) }}
                                    @if($task->comments != "")
                                    <br /><b>Comentarios</b><br />{{ $task->comments }}
                                    @endif
                                </p>
                                <form action="{{ action('TasksController@update', $task->id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <input type="hidden" name="status" value="COMPLETED">
                                    <button class="btn btn-success form-control"><i class="fa fa-check"></i> Marcar como completada</button>
                                </form>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Agregar Comentario</div>
                    <div class="panel-body">
                        <form action="{{ action('CommentsController@store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="lead_id" value="{{ $lead->id }}">
                            <div class="form-group">
                                <textarea name="comments" id="comments" cols="30" rows="3"
                                          class="form-control">{{ old('comments') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="file">Adjuntar Múltiples Archivos</label>
                                <input type="file" class="form-control" name="file[]" multiple >
                            </div>
                            <button class="btn btn-success form-control">
                                <i class="fa fa-comment"></i> Agregar Comentario
                            </button>
                        </form>
                    </div>
                </div>
                @foreach(\App\Comment::where('lead_id', $lead->id)->get() as $comment)
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>{{ $comment->comments }}</p>
                            @if($comment->file != "")
                                <hr>
                                <p class="text-right">
                                    <a target="_blank" href="{{ Storage::url('comments-files/' . $comment->file) }}" class="btn btn-default">
                                        <i class="fa fa-download"></i> Descargar archivo
                                    </a>
                                </p>
                            @endif
                            @if( count($comment->files) > 0 )
                            <hr>
                            <p class="lead">Archivos Adjuntos</p>
                            <div class="row">
                                @foreach($comment->files as $file)

                                <div class="col-xs-4 col-md-2">
                                    <a href="{{ $file->file }}" target="_blank">
                                        <img src="/img/folder.png" alt="Comment File" class="img-responsive">
                                    </a>
                                </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                        <div class="panel-footer">Creado en: {{ $comment->created_at }}</div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading"><i class="fa fa-check"></i> Tareas Completadas</div>
                    <div class="panel-body">
                        @foreach($lead->tasks->where('status', 'COMPLETADA') as $task)
                            <div class="panel panel-default">
                                <div class="panel-heading">{{ $task->title }}</div>
                                <div class="panel-body">
                                <div class="panel-body">
                                    <p>
                                        <b>Tipo:</b> {{ $task->type }}<br />
                                        <b>Fecha:</b> {{ date('M d, Y', strtotime($task->date)) }}<br />
                                        <b>Hora:</b> {{ date('h:i A', strtotime($task->time)) }}<br />
                                        <b>Comentarios:</b> {{ $task->comments }}<br />
                                        <b>Completada:</b> {{ $task->updated_at }}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection
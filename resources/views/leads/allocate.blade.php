@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-users"></i> Asignación de Contactos
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed" id="allocate-table">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Teléfono</th>
                            <th>País</th>
                            <th>WhatsApp</th>
                            <th>Idioma</th>
                            <th>Creado</th>
                            <th>Asignar</th>
                        </tr>
                        </thead>
                        {{--<tbody>--}}
                        {{--@foreach($leads as $lead)--}}
                        {{--<tr>--}}
                        {{--<th>{{ $lead->name }}</th>--}}
                        {{--<td>{{ $lead->email }}</td>--}}
                        {{--<td>{{ $lead->phone }}</td>--}}
                        {{--<td>{{ $lead->country }}</td>--}}
                        {{--<td class="text-center">{{ $lead->whatsapp }}</td>--}}
                        {{--<td>{{ $lead->language }}</td>--}}
                        {{--<td>{{ $lead->created_at }}</td>--}}
                        {{--<td>--}}
                        {{--<form action="{{ action('LeadsController@update', $lead->id) }}" method="post">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--{{ method_field('PATCH') }}--}}
                        {{--<input type="hidden" name="assign" value="1">--}}
                        {{--<div class="input-group">--}}
                        {{--<select name="user_id" id="user_id" class="form-control" required >--}}
                        {{--<option value="">Seleccione Asesor...</option>--}}
                        {{--@foreach(\App\User::orderBy('name')->get() as $user )--}}
                        {{--<option value="{{ $user->id }}">{{ $user->name }} ({{ count(\App\Lead::where('status', 'OPEN')->where('user_id', $user->id)->get()) }}) </option>--}}
                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--<span class="input-group-btn">--}}
                        {{--<button class="btn btn-success">--}}
                        {{--<i class="fa fa-arrow-circle-right"></i>--}}
                        {{--</button>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</td>--}}
                        {{--<td class="text-center">--}}
                        {{--<form onsubmit="return confirm('Are you sure?')" action="{{ action('LeadsController@destroy', $lead->id) }}" method="post">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--{{ method_field('DELETE') }}--}}
                        {{--<button class="btn btn-danger">--}}
                        {{--<i class="fa fa-trash"></i>--}}
                        {{--</button>--}}
                        {{--</form>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    </table>
                </div>
                {{--{{ $leads->render() }}--}}
            </div>
        </div>
    </div>

    @endsection

@section('js')

    <script>
        $(function() {
            $('#allocate-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! action('LeadsController@allocate_data') !!}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                    { data: 'country', name: 'country' },
                    { data: 'whatsapp', name: 'whatsapp' },
                    { data: 'language', name: 'language' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });
    </script>

    @endsection
@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro [video background] -->
        <section class="page-section page-section_intro _overlay-pattern">
            <div class="page-section_intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            @if(\Illuminate\Support\Facades\Cookie::get('lead') == 1)

                                @foreach($galleries as $gallery)
                                    <header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">{{ $gallery['name'] }}</span>
                                        <hr class="divider">
                                        <div class="row js-popup-gallery">
                                            @foreach($gallery['images'] as $image)
                                                <div class="col-md-4 col-xs-6">
                                                    <a href="/img/galleries/{{ $gallery['folder'] }}/{{ $image }}" class="js-popup-gallery">
                                                        <img src="/img/galleries/{{ $gallery['folder'] }}/{{ $image }}" alt="{{ $gallery['name'] }}" class="img-responsive img-before-after">
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                        <hr class="divider">
                                    </header>
                                @endforeach

                                @else

                                <header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">@lang('before-after.cta')</span>
                                    <hr class="divider">
                                </header>

                            @endif
                        </div>
                        <hr class="visible-xs" >
                        <div class="col-md-4 @if( \Illuminate\Support\Facades\Cookie::get('lead') != 1 ) hidden-xs @endif">
                            @include('partials._advisory')
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section intro [video background] -->

    </main>
    <!-- - PAGE MAIN -->

@endsection
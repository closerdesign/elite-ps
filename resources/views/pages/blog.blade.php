@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro -->
        <section class="page-section page-section_internal _overlay-black _background-image1 js-jarallax_type_1 _align-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="h1 page-section-internal__heading">Blog</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- section blog -->
        <section class="page-section page-section-blog page-section-blog_grid">
            <div class="container">
                <div class="row">
                    <!-- grid articles-->
                    <div class="col-md-8">
                        <div class="row js-blog-grid">
                            <div class="col-md-6 col-sm-6 js-blog-grid-sizer"></div>
                            @foreach($posts->data as $post)
                            <!-- article 1 -->
                            <article class="blog-article blog-article_preview col-md-6 col-sm-6 js-blog-grid-item blog-post"><a class="blog-article__image-link" href="{{ action('PagesController@post', $post->slug) }}"><img class="img-responsive" src="{{ $post->image }}" alt=""></a>
                                <div class="row blog-article-info">
                                    <time class="col-md-6 blog-article-info__date" datetime="2016-12-01"></time><a class="col-md-6 blog-article-info__category" href="{{ action('PagesController@blog') }}">blog</a>
                                </div>
                                <hr class="divider">
                                <div class="blog-article-content"><a class="blog-article-content__link-heading" href="{{ action('PagesController@post', $post->slug) }}">
                                        <h2 class="blog-article-content__heading">{{ $post->title }}</h2></a></div>
                            </article>
                            <!-- - article 1 -->
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <nav>
                                    <ul class="pagination pagination-sm">
                                        @if($posts->prev_page_url != null)
                                            <li>
                                                <a href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ explode("?", $posts->prev_page_url)[1] }}" aria-label="Previous"><span aria-hidden="true">@lang('pagination.previous')</span></a>
                                            </li>
                                        @endif

                                        @if($posts->next_page_url != null)
                                            <li>
                                                <a href="?@if(isset($_GET['keyword']))keyword={{ $_GET['keyword'] }}&@endif{{ explode("?", $posts->next_page_url)[1] }}" aria-label="Previous"><span aria-hidden="true">@lang('pagination.next')</span></a>
                                            </li>
                                        @endif
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- - grid articles-->
                    <!-- blog sidebar-->
                    <div class="col-md-4 @if( \Illuminate\Support\Facades\Cookie::get('lead') != 1 ) hidden-xs @endif">
                        @include('partials._advisory')
                    </div>
                    <!-- - blog sidebar-->
                </div>

            </div>
        </section>
    </main>
    <!-- - PAGE MAIN -->

    @endsection
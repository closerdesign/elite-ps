@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro [video background] -->
        <section class="page-section page-section_intro _overlay-pattern">
            <div class="page-section_intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">Colombia. El riesgo es que te quieras quedar.</span>
                                <hr class="divider">
                                <!-- 16:9 aspect ratio -->
                                <div class="embed-responsive embed-responsive-16by9 video-home">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VN0yGGcVii0?autoplay=1&rel=0"></iframe>
                                </div>
                            </header>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-md-4">
                            @include('partials._advisory')
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section intro [video background] -->

    </main>
    <!-- - PAGE MAIN -->

@endsection
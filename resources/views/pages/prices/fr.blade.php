@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro [video background] -->
        <section class="page-section page-section_intro _overlay-pattern">
            <div class="page-section_intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">@lang('prices.title')</span>
                                <hr class="divider">
                                <div class="row">
                                    <?php $pos = 0; ?>
                                    @foreach( json_decode(file_get_contents("https://closerdesign.net/api/gallery/18"))->images as $image )
                                        <div class="col-md-4">
                                            <img src="{{ $image->image }}" alt="" class="img-responsive">
                                        </div>
                                        <?php $pos++ ?>
                                    @endforeach
                                </div>
                                <hr class="divider">
                                <div class="row">
                                    @foreach( $products->products as $product )
                                        <div class="col-md-4">
                                            <div class="paquetes">
                                                <p><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/kxutvNFD5f8?autoplay=0&rel=0" style="width: 100%"></iframe></p>
                                                <h2>{{ $product->name }}</h2>
                                                <p class="product-name">{{ $product->short_description }}</p>
                                                {{--<p>(3) Cirugías deseadas DESDE {{ $prices[$region]['es']['package-1']['USD'] }} USD @if(isset($prices[$region]['es']['package-1']['EUR']) && $prices[$region]['es']['package-1']['EUR'] > 0) / {{ $prices[$region]['es']['package-1']['EUR'] }} EUR @endif</p>--}}
                                                <div class="product-description paquetes">
                                                    {!! $product->long_description !!}
                                                </div>
                                                <a href="#que-incluye" class="btn btn-primary form-control">@lang('prices.what-is-included')</a>
                                                <a class="btn btn-success form-control" target="_blank" href="https://wa.me/573152251608/?text=Quiero%20asesoria%20personalizada"> <i class="fa fa-whatsapp"></i> @lang('general.personal-assessment')</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </header>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-md-4">
                            @include('partials._advisory')
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section intro [video background] -->

        <!-- section about -->
        <section class="page-section page-section-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h2 class="h2 text-center">Other Packages</h2>
                        <ul class="list-group">
	                        <li class="list-group-item">Additional Day: 150 USD / 150 Euros.</li>
	                        <li class="list-group-item">Package 6 additional days: 500 USD / 500 Euros.</li>
	                        <li class="list-group-item">Abdominal Marking: 1000 USD / 1000 Euros.</li>
                            <li class="list-group-item">Abdominoplasty: 4500 USD / 4500 EUR.</li>
                            <li class="list-group-item">Blepharoplasty: 2800 USD / 2800 EUR.</li>
                            <li class="list-group-item">Otoplasty: 2800 USD / 2800 EUR.</li>
                            <li class="list-group-item">Bichectomy: 1000 USD / 1000 EUR.</li>
                            <li class="list-group-item">Jowl surgery: 1000 USD / 1000 EUR.</li>
                            <li class="list-group-item">Arms lift o Brachioplasty: 4500 USD / 5000 EUR.</li>
                            <li class="list-group-item">Back Lift: 4500 USD / 4500 EUR.</li>
                            <li class="list-group-item">Cruroplasty o Legs lift: 5000 USD / 5000 EUR.</li>
                            {{--<li class="list-group-item">Facial Lines Removal: 700 USD / 700 EUR.</li>--}}
                            <li class="list-group-item">Dewlap Liposuction: 700 USD / 700 EUR.</li>
                            <li class="list-group-item">Face lift: 6500 USD / 6500 EUR.</li>
                            <li class="list-group-item">Retiro de Biopolimeros: 6500 USD / 6500 EUR</li>
                            <li class="list-group-item">Implantes de Pectorales Masculinos: 6500 USD / 6500 EUR</li>
                            {{--<li class="list-group-item">Implante de Pantorrillas: 5500 USD / 5500 EUR</li>--}}
                            {{--<li class="list-group-item">Biopolimer Removal: 10500 USD / 10000 EUR</li>--}}
                            <li class="list-group-item">Implantes o Injerto Capilar: 4 USD / 4 EUR por injerto</li>
                            <li class="list-group-item">Bariatric Surgery (Bypass - Gastric Sleeve): 9800 USD / 9800 EUR</li>
                            <li class="list-group-item">Removal of biopolymers: 10500 USD / 10000 EUR</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section about -->

        <a name="whats-included"></a>

        <!-- section about -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="h2">What's included?</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <ul class="list-group">
                            <li class="list-group-item">Reception and Transfer Airport - Airport - Accommodation. Full style of 15 at 20 days (breakfast, lunch, dinner).</li>
                            <li class="list-group-item">Nursing service 24 hours during their stay.</li>
                            <li class="list-group-item">Foam board and Post Surgical.</li>
                            <li class="list-group-item">Pre-anesthetic consultation.</li>
                            <li class="list-group-item">Health insurance (Plastic Surgery Coverage).</li>
                            <li class="list-group-item">FDA approved implants.</li>
                            <li class="list-group-item">Surgical procedure.</li>
                            <li class="list-group-item">Plastic surgeon and specialist fees.</li>
                            <li class="list-group-item">Anesthesiology fees.</li>
                            <li class="list-group-item">Clinic and operation room.</li>
                            <li class="list-group-item">10 Postoperative massages.</li>
                            <li class="list-group-item">Post-surgical molding girdle or strip.</li>
                            <li class="list-group-item">Post-surgical diapers.</li>
                            <li class="list-group-item">Anti-trombo therapy stocking</li>
                            <li class="list-group-item">KIT of Basic Drugs.</li>
                            <li class="list-group-item">- Antibiotics</li>
                            <li class="list-group-item">- Anti inflammatory</li>
                            <li class="list-group-item">- Analgesics</li>
                            <li class="list-group-item">- Anticoagulants</li>
                            <li class="list-group-item"><b>(We don´t include additional or special drugs outside of the Basic KIT.)</b></li>
                            <li class="list-group-item">Post Surgical diapers</li>
                            <li class="list-group-item">Anti thrombus socks</li>
                            <li class="list-group-item">Health Coaching (Dietary advice).</li>
                            <li class="list-group-item">Laundry service</li>
                            <li class="list-group-item">International television signal.</li>
                            <li class="list-group-item">Wi-Fi</li>
                        </ul>

                    </div>

                    <div class="col-md-6">
	                    <!-- Seccion Obsequiamos -->
	                    <div class="panel panel-default">
                            <div class="panel-heading"><b><i class="fa fa-thumbs-up"></i> WE GIVE AWAY</b></div>
	                            <div class="panel-body">
	                                <ul class="list-group">
							        	<li class="list-group-item">* Extension of stay by medical order at no additional cost.</li>
							        	<li class="list-group-item">* Accompaniment Service for Tourist Tours.</li>
							        </ul>
	                            </div>
                        </div>

                        	                    <!-- Seccion Coordinadores -->
	                    <div class="panel panel-default">
                            <div class="panel-heading"><b><i class="fa fa-users"></i> IN ADDITION, WE ARE THE ONLY ONES WHO HAVE:</b></div>
                            <div class="panel-body">
                                <p class="text-justify"><b>Logistic coordinator:</b> Person in charge of managing all your pre and post operative process (reception, appointments, controls, consultations, exits, tours, complementary services).</p>
                                <p class="text-justify"><b>Customer Service Coordinator: </b> Person in charge of providing accompaniment and assistance in your accommodation to each and every one of your needs during your entire stay. </p>
                                <p class="text-justify"><b>Health and Wellbeing Coordinator: </b> Person in charge of providing follow-up to your pre and post-surgical process (delivery of post-surgical implements, belts, boards, foams, among others).</p>
                                <p class="text-justify"><b>Cleaning and general services coordinator: </b> Person in charge of ensuring the excellent state of cleanliness and sanitation of the recovery houses and giving management solutions to any eventuality in them.</p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"><b><i class="fa fa-bullhorn"></i> IMPORTANT PAYMENT INFORMATION</b></div>
                    <div class="panel-body">
                        <p class="text-justify">Payment will be made upon arrival, after you have been correctly hosted by your personal advisor. Our advisory team will let you know about our available payment methods and you should tell them about your preferred payment method before leaving your country.</p>
                        <p class="text-justify">Our Company cares about each patient's security and our goal is to provide you with a great experience, reason why you should send us your lab results at least one month in advance from the departure date. This should be done in your country.</p>
                        <p class="text-justify">- Credit / Debit Card (Visa, MasterCard, American Express) Additional Charge of 5% for taxes in Colombia.</p>
                        <p class="text-justify">- Bank Transfer (Bank Of America) Additional Charge of 5% for in Colombia.</p>
                        <p class="text-justify">- Cash payment</p>
                        <p class="text-justify">You must send your results to our advisor in order to check with our medical team about your health condition. Additionally, once you arrived, we’ll take new labs in Colombia.</p>
                        <p class="text-justify">Your friends and / or family members are always welcome! Each of them will be charged with just USD $100.00 / EUR €100.00 per night, including hosting, breakfast, lunch and dinner.</p>
                        <p class="text-justify"><b>Remember we are the # 1 Aesthetic Medical Tourism Company in Colombia. Do not hesitate, choose Security, medical excellence and all-inclusive prices and no hidden details. Avoid unnecessary risks and remember that your physical integrity and life cost much more! DO NOT expose yourself in the hands of just anyone, Leave yourself in the hands of real specialists.</b></p>

                        <p class="text-justify"><b>¡Contact us now!</b></p>
                        <p class="text-justify">Fill-out the form with your data below and make your wishes come true with the # 1 company in aesthetic medical tourism in Colombia.</b></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section about -->

    </main>
    <!-- - PAGE MAIN -->

@endsection

@section('js')

    <script>
        $(document).ready(function(){

            var box_height = 0;

            $('.product-description').each(function(){

                if( $(this).height() > box_height )
                {
                    box_height = $(this).height();
                }

            });

            var box_height = box_height + 20;

            if( $(window).width() > 414 )
            {
                $('.product-description').css('height', box_height + "px");
            }

        });
    </script>

@endsection
@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro [video background] -->
        <section class="page-section page-section_intro _overlay-pattern">
            <div class="page-section_intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">Precios y Paquetes</span>
                                <hr class="divider">
                                <div class="row">
                                    <?php $pos = 0; ?>
                                    @foreach( json_decode(file_get_contents("https://closerdesign.net/api/gallery/17"))->images as $image )
                                        <div class="col-md-4">
                                            <img src="{{ $image->image }}" alt="" class="img-responsive">
                                        </div>
                                        <?php $pos++ ?>
                                    @endforeach
                                </div>
                                <hr class="divider">
                                <div class="row">
                                    {{--{{ dd(json_decode(file_get_contents('https://closerdesign.net/api/product-category/168'))) }}--}}
                                    @foreach( $products->products as $product )
                                    <div class="col-md-4">
                                        <div class="paquetes">
                                            <p><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $product->yt_video }}?autoplay=0&rel=0" style="width: 100%"></iframe></p>
                                            <h2>{{ $product->name }}</h2>
                                            <p class="product-name">{{ $product->short_description }}</p>
                                            {{--<p>(3) Cirugías deseadas DESDE {{ $prices[$region]['es']['package-1']['USD'] }} USD @if(isset($prices[$region]['es']['package-1']['EUR']) && $prices[$region]['es']['package-1']['EUR'] > 0) / {{ $prices[$region]['es']['package-1']['EUR'] }} EUR @endif</p>--}}
                                            <div class="product-description paquetes">
                                                {!! $product->long_description !!}
                                            </div>
                                            <a href="#que-incluye" class="btn btn-primary form-control">@lang('prices.what-is-included')</a>
                                            <a class="btn btn-success form-control" target="_blank" href="https://wa.me/573152251608/?text=Quiero%20asesoria%20personalizada"> <i class="fa fa-whatsapp"></i> @lang('general.personal-assessment')</a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </header>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-md-4">
                            @include('partials._advisory')    
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section intro [video background] -->

        <!-- section about -->
        <section class="page-section page-section-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h2 class="h2 text-center">Otros Paquetes</h2>
                        <ul class="list-group">
	                        <li class="list-group-item">Día Adicional: 150 USD / 150 Euros.</li>
	                        <li class="list-group-item">Paquete 7 días Adicionales: 700 USD/ 700 Euros.</li>
	                        <li class="list-group-item">Marcación Abdominal: 1000 USD / 1000 Euros.</li>
                            <li class="list-group-item">Abdominoplastia: 4500 Dólares / 4500 Euros.</li>
                            <li class="list-group-item">Cirugía de Párpados: 3500 Dólares / 3500 Euros.</li>
                            <li class="list-group-item">Cirugía de Orejas: 3500 Dólares / 3500 Euros.</li>
                            <li class="list-group-item">Cirugía de Cachetes (Bichectomía): 1000 Dólares / 1000 Euros.</li>							
                            <li class="list-group-item">Cirugía de papada: 1000 Dólares / 1000 Euros.</li>
                            <li class="list-group-item">Cirugía de Brazos: 4500 Dólares / 4500 Euros.</li>
                            <li class="list-group-item">Cirugía de Espalda: 4500 Dólares / 4500 Euros.</li>
                            <li class="list-group-item">Cirugía de Piernas: 4500 Dólares / 4500 Euros.</li>
                            {{--<li class="list-group-item">Lifting Cérvico Facial: 6500 Dólares / 6500 Euros.</li>--}}
                            <li class="list-group-item">Implantes de Pectorales Masculinos: 6500 Dólares / 6500 Euros</li>
                            {{--<li class="list-group-item">Implante de Pantorrillas: 5500 Dólares / 5500 Euros</li>--}}
                            <li class="list-group-item">Implante de Pantorrillas: 5500 Dólares / 5500 Euros</li>
                            <li class="list-group-item">Implantes o Injerto Capilar: 4 Dólares / 4 Euros por injerto</li>
                            <li class="list-group-item">Cirugía Bariátrica (Bypass - Sleeve Gástrico): 9800 Dólares / 9800 Euros</li>
                            <li class="list-group-item">Retiro de Biopolimeros 9500 Dólares/ 9000 Euros.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section about -->
        
        <a name="que-incluye"></a>

        <!-- section about -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="h2">¿Qué incluye?</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <ul class="list-group">
                            <li class="list-group-item">Recibimiento y Traslados Aeropuerto - Alojamiento - Aeropuerto.</li>
                            <li class="list-group-item">Traslados Clínica - Consultorio Médico - Alojamiento</li>
                            <li class="list-group-item">Alojamiento - Hospedaje Full estilo de 15 a 20 d&iacute;as.</li>
                            <li class="list-group-item">2 Chefs Profesionales (Comida Internacional y Comida Caribeña) Desayuno, Almuerzo, Cena.</li>
                            <li class="list-group-item">Servicio de cuidados de enfermería las 24 horas durante su estadía.</li>
                            <li class="list-group-item">Consulta Pre anest&eacute;sica.</li>
                            <li class="list-group-item">Póliza de Seguro M&eacute;dico (Cobertura Cirugía Plástica).</li>
                            <li class="list-group-item">Procedimientos Quir&uacute;rgicos.</li>
                            <li class="list-group-item">Honorarios Cirujanos Pl&aacute;sticos y/o especialistas.</li>
                            <li class="list-group-item">Honorarios Anestesi&oacute;logo.</li>
                            <li class="list-group-item">Cl&iacute;nica / Sala de Cirug&iacute;a.</li>
                            <li class="list-group-item">10 Masajes, Drenajes Post-Operatorios.</li>
                            <li class="list-group-item">3 Controles Post- Quir&uacute;rgicos (Visitas al Medico).</li>
                            <li class="list-group-item">2 Fajas moldeadoras Post Quir&uacute;rgica.</li>
                            <li class="list-group-item">1 sostén o brasier.</li>
                            <li class="list-group-item">Tabla y Espuma Post Quir&uacute;rgica.</li>
                            <li class="list-group-item">KIT de medicamentos B&aacute;sicos.
                                <ul>
                                    <li>- Antibióticos</li>
                                    <li>- Antinflamatorios</li>
                                    <li>- Analgésicos</li>
                                    <li>- Anticoagulantes</li>
                                    <li><b>(NO incluimos medicamentos Adicionales o Especiales por fuera del KIT Básico.)</b></li>
                                </ul>
                            </li>
                            <li class="list-group-item">Pa&ntilde;ales Post Quir&uacute;rgicos.</li>
                            <li class="list-group-item">Medias anti trombos.</li>
                            <li class="list-group-item">Health Coaching (Asesoramiento nutricional).</li>
                            <li class="list-group-item">Servicio de Lavander&iacute;a.</li>
                            <li class="list-group-item">Televisi&oacute;n con se&ntilde;al internacional.</li>
                            <li class="list-group-item">Servicio Wi-Fi</li>
                            <li class="list-group-item"><b>Lo más importante : Te incluimos garantía Médica de Resultados Estéticos.</b></li>       	
                        </ul>
                    </div>
                    
                    <div class="col-md-6">
	                    
	                    <!-- Seccion Obsequiamos -->
	                    <div class="panel panel-default">
                            <div class="panel-heading"><b><i class="fa fa-thumbs-up"></i> OBSEQUIAMOS</b></div>
	                            <div class="panel-body">
	                                <ul class="list-group">
							        	<li class="list-group-item">* Extensión de estadía por orden médica sin costo adicional.</li>
							        	<li class="list-group-item">* Servicio de Acompañamiento para Tours Turísticos.</li>												    
						        	</ul>
	                            </div>
                        </div>
	                    
	                    <!-- Seccion Coordinadores -->
	                    <div class="panel panel-default">
                            <div class="panel-heading"><b><i class="fa fa-users"></i> ADEMAS SOMOS LOS UNICOS QUE CUENTAN CON:</b></div>
                            <div class="panel-body">
                                <p class="text-justify"><b>Coordinador Logístico:</b> Persona encargada de gestionar todo tu proceso pre y post operatorio (Recibimiento, citas, controles, consultas, salidas, tours, servicios complementarios).</p>
                                <p class="text-justify"><b>Coordinador de Servicio al Cliente: </b> Persona encargada de brindar acompañamiento y asistencia en tu alojamiento a todas y cada una de tus necesidades durante toda tu estancia.</p>
                                <p class="text-justify"><b>Coordinadora de Salud y Bienestar: </b> Persona encargada de brindar seguimiento a tu proceso pre y post quirúrgico (Entrega de implementos postquirúrgico, fajas, tablas, espumas, entre otros).</p>
                                <p class="text-justify"><b>Coordinador de Aseo y Servicios Generales: </b> Persona encargada de velar por el excelente estado de asepxia en las casas de recuperación y dar gestión solución a cualquier eventualidad en ellas.</p>
                            </div>
                        </div>                      
                    </div>
                </div>
                <!-- Seccion Importante -->
				<div class="row">
					<div class="col-md-12 col-md-offset-0">
						<div class="panel panel-default">
                            <div class="panel-heading"><b><i class="fa fa-bullhorn"></i> IMPORTANTE</b></div>
                            <div class="panel-body">
	                           	<p class="text-justify">El pago se realizará de forma personalizada a su llegada, una vez instalada en el sitio de alojamiento asignado por el equipo asesor.</p>
	                           	<p class="text-justify">El equipo asesor también le informará en la asesoría las formas de pago existentes y usted estará en la obligación de informarle la forma de pago escogida días antes de realizar su viaje.</p>
	                           	<p class="text-justify">- Tarjeta Crédito / Débito (Visa, MasterCard, American Express)
Cargo Adicional del 5% por impuestos de Ley en Colombia.</p>
	                           	<p class="text-justify">- Transferencia Bancaria (Bank Of América) Cargo Adicional del 5% por impuestos de Ley en Colombia.</p>
	                           	<p class="text-justify">- Pago en Efectivo (Cash).</p>
	                           	<p class="text-justify">Por la seguridad del paciente los exámenes médicos de laboratorio se deberán tomar con un mes de anticipo en el país de su residencia y previo a su llegada. Estos deben ser remitidos a su asesor personal para verificar con el equipo médico que su condición de salud es apta. Exámenes que serán tomados Obligatoriamente a su llegada a Colombia para su mayor seguridad.</p>
	                           	<p class="text-justify">Servicio de acompañante o familiar adicional 100 Dólares / 100 Euros por Noche incluido (Hospedaje, Desayuno, Almuerzo y Cena).</p>

                                <p class="text-justify"><b>Recuerde que somos # 1 en Turismo Medico Estético en Colombia, No dude más, escoja Seguridad, Excelencia médica y todo incluido sin Precios ni detalles ocultos, evite riesgos innecesarios y recuerde que su integridad física cuesta mucho y su vida más, NO la exponga en manos de cualquiera, déjala en manos de verdaderos especialistas.</b></p>

                                <p class="text-justify"><b>¡Comunícate Ya con nosotros!</b></p>
                                <p class="text-justify">Diligencia el formato con tus datos a continuación y haz tus deseos realidad con la <b>empresa # 1 en turismo médico estético en Colombia.</b></p>
                            </div>
                        </div>
					</div>
				</div>
            </div>
        </section>
        <!-- - section about -->
        

    </main>
    <!-- - PAGE MAIN -->

@endsection

@section('js')

    <script>
        $(document).ready(function(){

            var box_height = 0;

            $('.product-description').each(function(){

                if( $(this).height() > box_height )
                {
                    box_height = $(this).height();
                }

            });

            var box_height = box_height + 20;

            if( $(window).width() > 414 )
            {
                $('.product-description').css('height', box_height + "px");
            }

        });
    </script>

    @endsection
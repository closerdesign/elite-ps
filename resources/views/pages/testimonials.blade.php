@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro [video background] -->
        <section class="page-section page-section_intro _overlay-pattern">
            <div class="page-section_intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">@lang('testimonials.title')</span>
                                <hr class="divider">
                                <div class="row">
                                    @foreach($videos as $video)
                                    <div class="col-sm-4 col-md-4 col-xs-6">
                                        <div class="thumbnail">
                                            <a class="popup-youtube" href="http://www.youtube.com/watch?v={{ $video['video'] }}">
                                                <img style="background: url(http://img.youtube.com/vi/{{ $video['video'] }}/0.jpg); background-size: cover;" src="/img/video-bg.png" alt="{{ $video['country'] }}" class="img-responsive">
                                            </a>
                                            <div class="caption">
                                                <p style="margin-bottom: 0;">
                                                    <a href="http://www.youtube.com/watch?v={{ $video['video'] }}" class="btn btn-primary popup-youtube form-control" role="button"> <i class="fa fa-play-circle-o"></i> {{ $video['country'] }}</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </header>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-md-4">
                            @include('partials._advisory')
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section intro [video background] -->

    </main>
    <!-- - PAGE MAIN -->

@endsection
@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro -->
        <section class="page-section page-section_internal _overlay-black _background-image1 js-jarallax_type_1 _align-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="h1 page-section-internal__heading">{{ $post->title }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <!-- section blog -->
        <section class="page-section page-section-blog page-section-blog_grid">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <!-- article -->
                        <article class="blog-article">
                            <div class="blog-article-content">

                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                <div class="addthis_inline_share_toolbox"></div>

                                <img src="{{ $post->image }}" alt="{{ $post->title }}" class="img-responsive">

                                <hr class="divider">

                                {!! $post->content !!}

                                <hr class="divider">

                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                <div class="addthis_inline_share_toolbox"></div>

                            </div>
                        </article>
                        <!-- - article -->
                    </div>
                    <!-- blog sidebar-->
                    <div class="col-md-4 @if( \Illuminate\Support\Facades\Cookie::get('lead') != 1 ) hidden-xs @endif">
                        @include('partials._advisory')
                    </div>
                    <!-- - blog sidebar -->
                </div>
            </div>
        </section>
    </main>
    <!-- - PAGE MAIN -->

    @endsection
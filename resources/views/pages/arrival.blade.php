@extends('layouts.front')

@section('content')

    <!-- PAGE MAIN -->
    <main class="page-main">
        <!-- section intro [video background] -->
        <section class="page-section page-section_intro _overlay-pattern">
            <div class="page-section_intro__content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <header class="page-section_intro__header"><span class="page-section_intro__subheading _color-inverse">Recibimiento</span>
                                <hr class="divider">
                                <div class='embedsocial-album' data-ref="5613dac6e43efd1c48c5f97065a67aff0c8b65fa"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/ei.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialScript"));</script><iframe src="http://embedsocial.com/facebook_album/album_photos/223777447802004" width="100%" height="2200" frameborder="0" scrolling="no" marginheight="0"  marginwidth="0"></iframe>
                            </header>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-md-4">
                            @include('partials._advisory')
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- - section intro [video background] -->

    </main>
    <!-- - PAGE MAIN -->

@endsection
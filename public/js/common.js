"use strict";

// page preloader
$(window).on('load', function() {

	setTimeout(function() {
		$('.page-preloader').addClass('js-page-preloader_loaded');
	}, 125 );

	setTimeout(function() {
		$('.page-preloader').css('display', 'none');
	}, 250 );

});
// - page preloader


$(document).ready(function(){

	if (Modernizr.touch) {
		// better hover on mobile devices
        // run the forEach on each figure element
        [].slice.call(document.querySelectorAll('a, button')).forEach(function(el, i) {
            // check if the user moves a finger
            var fingerMove = false;
            el.addEventListener('touchmove', function(e) {
                e.stopPropagation();
                fingerMove = true;
            });
            // always reset fingerMove to false on touch start
            el.addEventListener('touchstart', function(e) {
                e.stopPropagation();
                fingerMove = false;
            });
        });
        // - better hover on mobile devices

        // responsive menu
		$('.list-menu__link_has-nested-menu').on('click', function(e) {
			e.preventDefault();
			$('.list-menu__link_has-nested-menu').removeClass('js-nested-menu-opened');
			$(this).toggleClass('js-nested-menu-opened');
		});
		// - responsive menu

    };

	// parallax init
	$('.js-jarallax_type_1').jarallax({
		type: "scroll",
		speed: 0.30
	});

	$('.js-jarallax_type_2').jarallax({
		type: "scroll",
		speed: 0.90
	});
	// - parallax init

	// init main plugins only after all images are loaded
	$('body').imagesLoaded(function(e) {
		$('.js-block-model_equal-height').equalHeights();

		$('.js-block-team-member_equal-height').equalHeights();

		var width = $(window).width();

		if( width > 768 )
		{
            $('.video-home').equalHeights();
            $('.contact-home').equalHeights();
            $('.blog-post').equalHeights();
		}

		$('.js-projects-grid').masonry({
			columnWidth: '.js-projects-grid-sizer',
			itemSelector: '.js-projects-grid-item',
			percentPosition: true
		});

		$('.js-models-grid').masonry({
			columnWidth: '.js-models-grid-sizer',
			itemSelector: '.js-models-grid-item',
			percentPosition: true
		});

		$('.js-blog-grid').masonry({
			columnWidth: '.js-blog-grid-sizer',
			itemSelector: '.js-blog-grid-item',
			percentPosition: true
		});		

		$('.js-carousel-testimonials').owlCarousel({
			autoplay: false,
			autoplaySpeed: 600,
			autoplayTimeout: 6000,
			autoplayHoverPause: true,
			autoHeight: true,
			loop: true,
			items: 1,
			nav: false,
			dots: true,
			dotsContainer: '.js-testimonials-controls'
		});
	});

	// particles.js
	if ($('#particles-js').length) {
		particlesJS.load('particles-js', 'js/particlesjs-config.json');
	}
	// - particles.js

	// video background
	$('#js-video-background').YTPlayer({
		fitToBackground: true,
		loop: true,
		videoId: 'skIizzkt3xQ',
		quality: 'small',
		playerVars: {
			modestbranding: 0,
			autoplay: 1,
			controls: 0,
			showinfo: 0,
			branding: 0,
			rel: 0,
			autohide: 0,
			start: 35
		}
	});	
	// - video background

	// ignore clicking on nested menu link
	$('.menu-responsive-button').on('click', function(e) {
		e.preventDefault();
		$('.btn-home').hide();
		$('.home-option').fadeIn();
		$('.menu-responsive-button').hide();
		$('.menu-responsive-button').removeClass('visible-xs');
		$('.close-responsive-button').show();
		$('.close-responsive-button').style('display', 'block');
	});
	// - ignore clicking on nested menu link

	// ignore clicking on nested menu link
	$('.close-responsive-button').on('click', function(e) {
		e.preventDefault();
		// $('.btn-home').hide();
		$('.menu-responsive-button').show();
		$('.menu-responsive-button').addClass('visible-xs');
		$('.close-responsive-button').hide();
		$('.home-option').hide();
	});
	// - ignore clicking on nested menu link



	// magnific gallery
	$('.js-popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
	});
	// - magnific gallery

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
});

// Colored Navigation
$('.btn-home').click(function(){

    $('.btn-home').hide();
    $('.home-option').fadeIn();

});

$('.lang_selection').click(function(){
	$('.lang-box').fadeIn();
});

$('.lang-selection-close').click(function(){
	$('.lang-box').fadeOut();
});

$(document).ready(function(){

    $(window).scroll(function() {
        if ($(this).scrollTop() > 30) { // this refers to window
            $('#whatsapp-cta').css('position', 'fixed').css('width', '100%').css('z-index', '99999').css('top', '0');
        }else{
            $('#whatsapp-cta').css('position', 'relative').css('width', '100%').css('z-index', '99999');
        }
    });

});


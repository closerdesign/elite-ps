const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
    'public/bower_components/bootstrap/dist/css/bootstrap.min.css',
    'public/css/magnific-popup.css',
    'public/css/font-awesome.min.css',
    'public/css/freepik.css',
    'public/css/owl.carousel.min.css',
    'public/style.css'
], 'public/all.css');

mix.js([
    "public/js/modernizr.custom.js",
    "public/js/jquery.min.js",
    "public/js/imagesloaded.pkgd.min.js",
    "public/js/masonry.pkgd.min.js",
    "public/js/jarallax.min.js",
    "public/js/jquery.youtubebackground.js",
    "public/js/jquery.equalheights.min.js",
    "public/js/owl.carousel.min.js",
    "public/js/jquery.magnific-popup.min.js",
    "public/js/particles.min.js"
    ], 'public/all.js')
   .sass('resources/assets/sass/app.scss', 'public/css');

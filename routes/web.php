<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('en', function (){
    \Illuminate\Support\Facades\Session::put('language', 'en');
    return back();
});

Route::get('es', function (){
    \Illuminate\Support\Facades\Session::put('language', 'es');
    return back();
});

Route::get('fr', function (){
    \Illuminate\Support\Facades\Session::put('language', 'fr');
    return back();
});

/**
 *  Additional Sites
 *
 *
 */

$sites_routes = function(){

    Route::get('/', 'SitesController@index');
    Route::get('antes-y-despues', 'SitesController@before_after');
    Route::get('contacto', 'SitesController@contact');

};

Route::group(['domain' => 'cirugiaplasticabogota.test'], $sites_routes);
Route::group(['domain' => 'cirugiaplasticabogota.org'], $sites_routes);

Route::group(['domain' => 'elitecirugiaplastica.com.co'], function(){

    Route::get('/', 'PagesController@home');

});

/**
 *  Pages
 *
 *
 */

Route::get('/', 'PagesController@home');
Route::get('testimonials', 'PagesController@testimonials');
Route::get('before-after', 'PagesController@before_after');
Route::get('prices', 'PagesController@prices');
Route::get('precios', 'PagesController@prices');
Route::get('region/{id}', 'PagesController@region');
Route::get('colombia', 'PagesController@colombia');
Route::get('contact', 'PagesController@contact');
Route::get('contacto', 'PagesController@contact');
Route::get('about', 'PagesController@about');
Route::get('why-elite', 'PagesController@why_elite');
Route::get('security', 'PagesController@security');
Route::get('arrival', 'PagesController@arrival');
Route::get('hosting', 'PagesController@hosting');
Route::get('advisors', 'PagesController@advisors');
Route::get('blog', 'PagesController@blog');
Route::get('blog/{slug}', 'PagesController@post');

/**
 *  CRM
 *
 *
 */

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/{status?}', 'HomeController@index');

Route::resource('comment', 'CommentsController');
Route::get('lead/allocate', 'LeadsController@allocate');
Route::get('lead/allocate_data', 'LeadsController@allocate_data');
Route::get('lead/assignee', 'LeadsController@assignee');
Route::get('lead/report', 'LeadsController@report');
Route::get('lead/vcard/{id}', 'LeadsController@vcard');
Route::post('lead/search', 'LeadsController@search');
Route::resource('lead', 'LeadsController');
Route::get('task/summary', 'TasksController@summary');
Route::resource('task', 'TasksController');

Route::get('tutorial', 'LeadsController@tutorial');

/**
 *  Admin
 *
 *
 */

Route::group(['prefix' => 'admin', 'midleware' => 'admin'], function(){

    Route::get('transfer', 'AdminController@transfer_view');
    Route::post('transfer', 'AdminController@transfer');

    Route::get('switch-user/view', 'AdminController@switch_user_view');
    Route::get('switch-user/{id}', 'AdminController@switch_user');

    Route::get('users', 'AdminController@users');
    Route::get('user-details/{id}', 'AdminController@user_details');
    Route::post('user-update/{id}', 'AdminController@user_update');
    Route::post('user-delete/{id}', 'AdminController@user_delete');

});

/**
 * Forms
 *
 *
 */

Route::post('leads', 'FormsController@leads');

/**
 *  Landing Pages
 *
 *
 */

Route::group(['prefix' => 'landings'], function(){

    Route::get('puerto-rico', 'LandingsController@puerto_rico');

});